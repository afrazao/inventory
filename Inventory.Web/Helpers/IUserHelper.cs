﻿using Inventory.Web.Data.Entities;
using Inventory.Web.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Helpers
{
    public interface IUserHelper
    {


        IQueryable GetAllUsers();

        Task<User> GetUserByEmailAsync(string email);

        Task<IdentityResult> AddUserAsync(User user, string password);

        Task<SignInResult> LoginAsync(LoginViewModel model);

        Task LogoutAsync();

        Task<IdentityResult> UpdateUserAsync(User user);

        Task<IdentityResult> ChangePasswordAsync(User user, string oldpassword, string newpassword);

        Task<SignInResult> ValidatePasswordAsync(User user, string password); //É para ver se a password é válida

        Task CheckRoleAsync(string roleName);

        Task AddUserToRoleAsync(User user, string roleName);

        Task<bool> IsUserInRoleAsync(User user, string roleName);
        Task<User> GetUserByIdAsync(string userId);
        Task<IdentityResult> GetConfirmEmailAsync(User user, string code);
        Task<string> GetGenerateEmailConfirmationTokenAsync(User user);
        Task<bool> IsEmailConfirmedAsync(User user);
        Task<string> GetGeneratePasswordResetTokenAsync(User user);
        User GetUserByEmail(string email);
        Task<IdentityResult> ResetPasswordAsync(User user, string token, string newpassword);
        IQueryable GetRoles();
        Task RemoveUserFromRoleAsync(User user, string roleName);
        List<IdentityUserRole<string>> GetViewerRoles(string username);
        string GetRoleName(string id);
    }
}
