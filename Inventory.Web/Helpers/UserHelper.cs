﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Models;
using Microsoft.AspNetCore.Identity;

namespace Inventory.Web.Helpers
{
    public class UserHelper : IUserHelper
    {
        private readonly UserManager<User> userManager;
        private readonly SignInManager<User> signInManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly DataContext dataContext;

        public UserHelper(UserManager<User> userManager, SignInManager<User> signInManager, RoleManager<IdentityRole> roleManager, DataContext dataContext)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
            this.dataContext = dataContext;
        }

        public async Task<IdentityResult> AddUserAsync(User user, string password)
        {
            return await this.userManager.CreateAsync(user, password);
        }

        public async Task AddUserToRoleAsync(User user, string roleName)
        {
            await this.userManager.AddToRoleAsync(user, roleName);
        }

        public async Task RemoveUserFromRoleAsync(User user, string roleName)
        {
            await this.userManager.RemoveFromRoleAsync(user, roleName);
        }

        public async Task<IdentityResult> ChangePasswordAsync(User user, string oldpassword, string newpassword)
        {
            return await this.userManager.ChangePasswordAsync(user, oldpassword, newpassword);
        }

        public async Task<IdentityResult> ResetPasswordAsync(User user, string token, string newpassword)
        {
            return await this.userManager.ResetPasswordAsync(user, token, newpassword);
        }

        public async Task CheckRoleAsync(string roleName)
        {
            var roleExists = await this.roleManager.RoleExistsAsync(roleName);
            if (!roleExists)
            {
                await this.roleManager.CreateAsync(new IdentityRole
                {
                    Name = roleName
                });
            }
        }

        public async Task<User> GetUserByEmailAsync(string email)
        {
            return await this.userManager.FindByEmailAsync(email);
        }

        public async Task<User> GetUserByIdAsync(string userId)
        {
            return await this.userManager.FindByIdAsync(userId);
        }

        public async Task<bool> IsUserInRoleAsync(User user, string roleName)
        {
            return await this.userManager.IsInRoleAsync(user, roleName);
        }

        public async Task<SignInResult> LoginAsync(LoginViewModel model)
        {
            return await this.signInManager.PasswordSignInAsync(
                model.UserName,
                model.Password,
                model.RememberMe,
                false);
        }

        public async Task LogoutAsync()
        {
            await this.signInManager.SignOutAsync();
        }

        public async Task<IdentityResult> UpdateUserAsync(User user)
        {
            return await this.userManager.UpdateAsync(user);
        }

        public async Task<SignInResult> ValidatePasswordAsync(User user, string password)
        {
            return await this.signInManager.CheckPasswordSignInAsync(user, password, false);
        }

        public async Task<IdentityResult> GetConfirmEmailAsync(User user, string code)
        {
            return await this.userManager.ConfirmEmailAsync(user, code);
        }

        public async Task<string> GetGenerateEmailConfirmationTokenAsync(User user)
        {
            return await this.userManager.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<bool> IsEmailConfirmedAsync(User user)
        {
            return await this.userManager.IsEmailConfirmedAsync(user);
        }

        public async Task<string> GetGeneratePasswordResetTokenAsync(User user)
        {
            return await this.userManager.GeneratePasswordResetTokenAsync(user);
        }

        public IQueryable GetAllUsers()
        {
            return this.dataContext.Users;
        }

        public User GetUserByEmail(string email)
        {
            return this.dataContext.Users.Where(u => u.UserName == email).FirstOrDefault();
        }


        public IQueryable GetRoles()
        {
            var roles = roleManager.Roles;

            return roles;
        }

        public List<IdentityUserRole<string>> GetViewerRoles(string username)
        {
            var userInfo = this.dataContext.Users.Where(u => u.Email == username).Select(u => u.Id).FirstOrDefault();

            var roles = this.dataContext.UserRoles.Where(r => r.UserId == userInfo).ToList();

            return roles;
        }

        public string GetRoleName(string id)
        {
            return this.dataContext.Roles.Where(r => r.Id == id).Select(r => r.Name).FirstOrDefault();
        }
    }
}
