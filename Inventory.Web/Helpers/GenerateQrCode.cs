﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Helpers
{
    public static class GenerateQrCode
    {
        public static QRCode GetQRCode(string data)
        {
            QRCodeGenerator qr = new QRCodeGenerator();
            QRCodeData create = qr.CreateQrCode(data, QRCodeGenerator.ECCLevel.Q);
            QRCode code = new QRCode(create);
            return code;
           
        }
    }
}
