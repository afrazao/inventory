﻿using Inventory.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Helpers
{
    public static class PermissionsHelper
    {
        public static List<Permissions> PermissionsList()
        {
            List<Permissions> list = new List<Permissions>();

            list = Enum.GetValues(typeof(Permissions)).Cast<Permissions>().ToList();

            return list;
        }
    }
}
