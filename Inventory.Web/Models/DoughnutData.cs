﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Models
{
    public class DoughnutData
    {
        public string xValue;
        public double yValue;
        public string text;
    }
}
