﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Models
{
    public enum Permissions
    {
        InventoryCRU,

        InventoryView
    }
}
