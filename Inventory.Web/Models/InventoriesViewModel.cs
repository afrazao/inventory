﻿using Inventory.Web.Data.Entities;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;

namespace Inventory.Web.Models
{
    public class InventoriesViewModel : AInventory
    {

        [Display(Name = "Image")]
        public IFormFile ImageFile { get; set; }
    }
}
