﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Models
{
    public class EditInventoryStateViewModel
    {
        [Required]
        [Display(Name = "Inventory Name")]
        public string InventoryName { get; set; }

        [Required]
        [Display(Name = "State")]
        public int StateId { get; set; }
    }
}
