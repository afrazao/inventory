﻿
using Inventory.Web.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Inventory.Web.Services
{
    public class ApiService
    {

        public static async Task<Response> GetCities(string urlBase, string controller)
        {
            try
            {
                var client = new HttpClient(); //http para fazer a ligação externa
                client.BaseAddress = new Uri(urlBase); //base address da api
                var responde = await client.GetAsync(controller); //para ir buscar o controlador da api

                var result = await responde.Content.ReadAsStringAsync(); //para ler o conteudo da response como string;carregar os resultados do formato 
                                                                         //string para a variavel response

                if (!responde.IsSuccessStatusCode) //se correr mal
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result

                    };
                }

                var zipCode = JsonConvert.DeserializeObject<Codigo>(result);


                return new Response
                {
                    IsSuccess = true,
                    Result = zipCode
                };
            }
            catch (Exception ex)

            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }


        public static async Task<Response> GetCitiesGPS(string urlBase, string controller)
        {
            try
            {
                var client = new HttpClient(); //http para fazer a ligação externa
                client.BaseAddress = new Uri(urlBase); //base address da api
                var responde = await client.GetAsync(controller); //para ir buscar o controlador da api

                var result = await responde.Content.ReadAsStringAsync(); //para ler o conteudo da response como string;carregar os resultados do formato 
                                                                         //string para a variavel response

                if (!responde.IsSuccessStatusCode) //se correr mal
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result

                    };
                }

                var gps = JsonConvert.DeserializeObject<CodigoComGps>(result);


                return new Response
                {
                    IsSuccess = true,
                    Result = gps
                };
            }
            catch (Exception ex)

            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
