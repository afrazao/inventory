﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Data.Entities
{
    public class InventoryArticle:IEntity
    {
        public int Id { get; set; }

        public AInventory Inventory { get; set; }

        [Required]
        public int AInventoryId { get; set; }

        public Article Article { get; set; }

        [Required]
        public int ArticleId { get; set; }

        [Required]
        public decimal Quantity { get; set; }

      
        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string Unit { get; set; }

     
        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string Name { get; set; }

      
    }
}
