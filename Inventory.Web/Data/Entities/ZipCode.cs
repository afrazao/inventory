﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Data.Entities
{
    public class ZipCode:IEntity
    {
        public int Id { get; set; }

        [StringLength(8, ErrorMessage = "The {0} must have {1} characters")]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string Code { get; set; }

        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string City { get; set; }

        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string Country { get; set; }

      
        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        [Display(Name = "Street")]
        public string Name { get; set; }

        public string longitude { get; set; }

        public string latitude { get; set; }

    }
}
