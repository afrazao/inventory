﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Data.Entities
{
    public class ViewerInventories : IEntity
    {
        public int Id { get; set; }

        [Required]
        public int AInventoryId { get; set; }

        public AInventory Inventory { get; set; }

        [Required]
        public int ViewerId { get; set; }

        public Viewer Viewer { get; set; }


        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string Name { get; set; }
    }
}
