﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Data.Entities
{
    public class Article:IEntity
    {
        public int Id { get; set; }

        public Category Category { get; set; }

        [Required]
        public int CategoryId { get; set; }

        [Required]
        public string Code { get; set; }

     
        public string Address { get; set; }



        public  byte[] BarCode { get; set; }

     
        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string Name { get; set; }

        //Todo:por imagens e editar controlador
       
        public ZipCode ZipCode { get; set; }

        [Required]
        public int ZipCodeId { get; set; }

        [Display(Name = "Image")]
        public string ImageUrl { get; set; }

        public string ImageFullPath
        {
            get
            {
                if (string.IsNullOrEmpty(this.ImageUrl))
                {
                    return null;
                }
                //TODO:não esquecer de mudar isto quando se muda o servidor e para https
                return $" https://inventorywebnow.azurewebsites.net{this.ImageUrl.Substring(1)}";
            }
        }
    }
}
