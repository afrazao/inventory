﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Data.Entities
{
    public class SubscriptionType:IEntity
    {
        public int Id { get; set; }

        //public string Type { get; set; }
        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string Name { get; set; }


        [Required]
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public decimal Price { get; set; }

        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string Space { get; set; }

        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string CustomerService { get; set; }
    }
}
