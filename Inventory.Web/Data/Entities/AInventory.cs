﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Data.Entities
{
    public class AInventory:IEntity
    {
        public int Id { get; set; }

        [Required]
        public int ClientId { get; set; }

        public Client Client { get; set; }

      
      
     
        public DateTime Date { get; set; }
  
        public State State { get; set; }

        [Required]
        public int StateId { get; set; }

        public InventoryType InventoryType { get; set; }

        [Required]
        public int InventoryTypeId { get; set; }

      
        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string Name { get; set; }

        [Display(Name ="Image")]
     
        public string ImageUrl { get; set; }


        public string ImageFullPath
        {
            get
            {
                if(string.IsNullOrEmpty(this.ImageUrl))
                {
                    return null;
                }
                //TODO:não esquecer de mudar isto quando se muda o servidor e para https
                return $" https://inventorywebnow.azurewebsites.net{this.ImageUrl.Substring(1)}";
            }
        }

        
    }
}
