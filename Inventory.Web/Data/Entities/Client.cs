﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Data.Entities
{
    public class Client : IEntity
    {
    
        public int Id { get; set; } 

       
        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string Name { get; set; }


        [Display(Name = "Last Name")]
        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string LastName { get; set; }

        public string UserId { get; set; }

        public User User { get; set; }

      //TODO:colocar imagem e editar controlador

        [Required(ErrorMessage = "You must insert a {0}")]
        [StringLength(100, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 3)]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Required]
        public int ZipCodeId { get; set; }
 
        public ZipCode ZipCode { get; set; }    

        //[StringLength(9, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 9)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public int TIN { get; set; }

        public State State { get; set; } //é suposto ligar a outra tabela

        [Required]
        public int StateId { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

       
        [Required(ErrorMessage = "You must insert a {0}")]
        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 9)]
        [Display(Name = "Phone Number")]
        public string Contact { get; set; }

        public SubscriptionType SubscriptionType { get; set; }

        [Required]
        public int SubscriptionTypeId { get; set; }



        [Display(Name = "Image")]
        public string ImageUrl { get; set; }


        public string ImageFullPath
        {
            get
            {
                if (string.IsNullOrEmpty(this.ImageUrl))
                {
                    return null;
                }
                //TODO:não esquecer de mudar isto quando se muda o servidor e para https
                return $" https://inventorywebnow.azurewebsites.net{this.ImageUrl.Substring(1)}";
            }
        }
    }
}
