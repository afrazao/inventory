﻿using Inventory.Web.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Data.Entities
{
    public class Viewer: IEntity
    {
        public int Id { get; set; }

      
        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string Name { get; set; }

        [Display(Name ="Last Name")]
        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 2)]
        [Required(ErrorMessage = "You must insert a {0}")]
        public string LastName { get; set; }

        [Required]
        public int ClientId { get; set; }

        public User User { get; set; }

        public string UserId { get; set; }

        public Client Client { get; set; }

        [Required]
        public Permissions Permission { get; set; }

      
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "You must insert a {0}")]
        [StringLength(30, ErrorMessage = "The {0} must have between {2} and {1} characters", MinimumLength = 9)]
        [Display(Name = "Phone Number")]
        public string Contact { get; set; }
    }
}
