﻿

namespace Inventory.Web.Data.Repositories
{
    using Inventory.Web.Data.Entities;
    using Inventory.Web.Data.IRepositories;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class ArticleRepository : GenericRepository<Article>, IArticleRepository
    {
        private readonly DataContext context;

        //TODO:perceber se este esta bem
        public ArticleRepository(DataContext context) : base(context)
        {
            this.context = context;
        }


        public List<Article> GetArticles()
        {
            return this.context.Articles.ToList();
        }

        public IQueryable GetAllInclude()
        {
            return this.context.Articles.Include(a => a.Category).Include(a => a.ZipCode);
        }


        public async Task<Article> GetByIdAsyncInclude(int id)
        {
            return await this.context.Articles.Where(i => i.Id == id).Include(a => a.Category).Include(a => a.ZipCode).FirstOrDefaultAsync();
        }

        public int GetArticleId(string barcode)
        {
            return this.context.Articles.Where(a => a.Code == barcode).Select(a => a.Id).FirstOrDefault();
        }

        public List<Article> GetArticlesByInventoryArticlesId(List<int> idList)
        {
            List<Article> ArticlesList = new List<Article>();

            foreach (var item in idList)
            {
                ArticlesList.Add(this.context.Articles.Where(v => v.Id == item).Select(v => v).FirstOrDefault());
            }

            return ArticlesList;
        }


        public List<Article> GetArticlesByCategory(List<Article> list, int categoryid)
        {
            List<Article> listToSend = new List<Article>();

            foreach(var item in list)
            {
                if (item.CategoryId == categoryid)
                {
                    listToSend.Add(item);
                }
            }

            return listToSend;
        }

        public Article FindArticleByCode(string code)
        {
            return this.context.Articles.Where(a => a.Code == code).FirstOrDefault();
        }

        public string GetArticleAddressById(int articleId)
        {
            var address = this.context.Articles.Where(a => a.Id == articleId).Select(a => a.Address).FirstOrDefault();
            return address;
        }
    }
}
