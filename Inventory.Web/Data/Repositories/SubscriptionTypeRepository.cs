﻿


namespace Inventory.Web.Data.Repositories
{
    using Inventory.Web.Data.Entities;
    using Inventory.Web.Data.IRepositories;
    public class SubscriptionTypeRepository : GenericRepository<SubscriptionType>, ISubscriptionTypeRepository
    {
        public SubscriptionTypeRepository(DataContext context) : base(context)
        {

        }
    }
}
