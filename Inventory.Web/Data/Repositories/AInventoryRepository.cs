﻿

namespace Inventory.Web.Data.Repositories
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Inventory.Web.Data.Entities;
    using Inventory.Web.Data.IRepositories;
    using Microsoft.EntityFrameworkCore;

    public class AInventoryRepository : GenericRepository<AInventory>, IAInventoryRepository
    {

        private readonly DataContext context;

        public AInventoryRepository(DataContext context) : base(context)
        {
            this.context = context;
        }

        /// <summary>
        /// retorna todos os inventarios de um determinado cliente
        /// </summary>
        /// <returns></returns>
        public IQueryable GetAllWithClients()
        {
            return this.context.Inventories.Include(i => i.Client).Include(a => a.InventoryType).Include(a => a.State);
        }

        public List<AInventory> GetInventoryListById(int id)
        {
            return this.context.Inventories.Where(i => i.Id == id).Include(i => i.Client).Include(a => a.InventoryType).Include(a => a.State).ToList();
        }

        public IQueryable GetThisClientInventories(string email)
        {
            return this.context.Inventories.Where(e => e.Client.Email == email).Include(a => a.InventoryType).Include(a => a.State);
        }

        public async Task<AInventory> GetByIdAsyncInclude(int id)
        {
            return await this.context.Inventories.Where(i => i.Id == id).Include(a => a.Client)
                 .Include(a => a.InventoryType)
                 .Include(a => a.State).FirstOrDefaultAsync();
        }

        public List<AInventory> GetInventoriesListByClient(string email)
        {
            return this.context.Inventories.Where(e => e.Client.Email == email).Include(a => a.InventoryType).Include(a => a.State).Include(a => a.Client).ToList();
        }

        //************ InventoryArticles ************************

        public string InventoryName(int inventoryId)
        {
            var inventory = this.context.Inventories.Where(i => i.Id == inventoryId).FirstOrDefault();
            return inventory.Name;
        }


        public IQueryable GetThisInventoryInventoryArticles(int inventoryId)
        {

            return this.context.InventoryArticles.Where(i => i.AInventoryId == inventoryId).Include(i => i.Article).Include(i => i.Inventory); 
        }

        public InventoryArticle GetInventoryArticleByName(string name)
        {
            return this.context.InventoryArticles.Where(s => s.Name == name).FirstOrDefault();
        }

        public string GetInventoryArticleName(int id)
        {
            return this.context.InventoryArticles.Where(s => s.Id == id).Select(s => s.Name).FirstOrDefault();
        }


        public IQueryable GetInventoryArticles()
        {
            return this.context.InventoryArticles.Include(i => i.Article).Include(i => i.Inventory);
        }

        public async Task<InventoryArticle> GetInventoryArticleById(int id)
        {
            return await this.context.InventoryArticles.Where(i => i.Id == id).Include(i => i.Article).Include(i => i.Inventory).FirstOrDefaultAsync();
        }

        public async Task<InventoryArticle> CreateInventoryArticleAsync(InventoryArticle inventoryArticle)
        {
            await this.context.InventoryArticles.AddAsync(inventoryArticle);
            await SaveAllOthersAsync();
            return inventoryArticle;
        }

        public async Task<InventoryArticle> UpdateInventoryArticleAsync(InventoryArticle inventoryArticle)
        {
            this.context.InventoryArticles.Update(inventoryArticle);
            await SaveAllOthersAsync();
            return inventoryArticle;
        }

        public async Task DeleteInventoryArticleAsync(InventoryArticle inventoryArticle)
        {
            this.context.InventoryArticles.Remove(inventoryArticle);
            await SaveAllOthersAsync();

        }


        public async Task<bool> InventoryArticleExistsAsync(int id)
        {
            return await this.context.InventoryArticles.AnyAsync(e => e.Id == id);
        }

        public List<InventoryArticle> GetInventoryArticles(int id)
        {
            return this.context.InventoryArticles.Where(a => a.AInventoryId == id).ToList();
        }

        public InventoryArticle GetInventoryArticleByArticleId(int id)
        {
            return this.context.InventoryArticles.Where(a => a.ArticleId == id).FirstOrDefault();
        }

        //************ States ************************


        public State GetStateByName(string name)
        {
            return this.context.States.Where(s => s.Name == name).FirstOrDefault();
        }

        public string GetStateName(int id)
        {
            return this.context.States.Where(s => s.Id == id).Select(s => s.Name).FirstOrDefault();
        }

        public int GetStateIdByName(string name)
        {
            return this.context.States.Where(s => s.Name == name).Select(s => s.Id).FirstOrDefault();
        }

        public IQueryable GetStates()
        {
            return this.context.States;
        }

        public async Task<State> GetStateById(int id)
        {
            return await this.context.States.Where(i => i.Id == id).FirstOrDefaultAsync();
        }


        public async Task<State> CreateStateAsync(State state)
        {
            await this.context.States.AddAsync(state);
            await SaveAllOthersAsync();
            return state;
        }

        public async Task<State> UpdateStateAsync(State state)
        {
            this.context.States.Update(state);
            await SaveAllOthersAsync();
            return state;
        }

        public async Task DeleteStateAsync(State state)
        {
            this.context.States.Remove(state);
            await SaveAllOthersAsync();

        }


        public async Task<bool> StateExistsAsync(int id)
        {
            return await this.context.States.AnyAsync(e => e.Id == id);
        }



        //******************Inventory Types ***********************

        public IQueryable GetInventoryTypes()
        {
            return this.context.InventoryTypes;
        }

        public async Task<InventoryType> GetInventoryTypeById(int id)
        {
            return await  this.context.InventoryTypes.Where(i => i.Id == id).FirstOrDefaultAsync();
        }


        public async Task<InventoryType> CreateInventoryTypeAsync(InventoryType inventoryType)
        {
            await this.context.InventoryTypes.AddAsync(inventoryType);
            await SaveAllOthersAsync();
            return inventoryType;
        }



        public async Task<InventoryType> UpdateInventoryTypeAsync(InventoryType inventoryType)
        {
            this.context.InventoryTypes.Update(inventoryType);
            await SaveAllOthersAsync();
            return inventoryType;
        }


        public async Task  DeleteInventoryTypeAsync(InventoryType inventoryType)
        {
            this.context.InventoryTypes.Remove(inventoryType);
            await SaveAllOthersAsync();
           
        }


        public async Task<bool> InventoryTypeExistsAsync(int id)
        {
            return await this.context.InventoryTypes.AnyAsync(e => e.Id == id);
        }

        public async Task<bool> SaveAllOthersAsync()
        {
            return await this.context.SaveChangesAsync() > 0;
        }

        public List<AInventory> GetInventoriesByViewer(List<int> list)
        {
            List<AInventory> listToSend = new List<AInventory>();

            foreach (var item in list)
            {
                listToSend.Add(this.context.Inventories.Where(i => i.Id == item).Include(a => a.InventoryType).Include(a => a.State).FirstOrDefault());
            }

            return listToSend;
        }

        public void OnlyUpdateInventoryState(int id, int stateId)
        {
            var inventoryToChange = this.context.Inventories.Where(i => i.Id == id).SingleOrDefault();
            inventoryToChange.StateId = stateId;
            context.Entry(inventoryToChange).Property("StateId").IsModified = true;
            context.SaveChanges();
        }

    }
}
