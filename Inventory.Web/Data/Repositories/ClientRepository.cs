﻿


namespace Inventory.Web.Data.Repositories
{
    using Inventory.Web.Data.Entities;
    using Inventory.Web.Models;
    using IRepositories;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class ClientRepository : GenericRepository<Client>, IClientRepository
    {
        private readonly DataContext context;

        public ClientRepository(DataContext context) : base(context)
        {
            this.context = context;
        }

        public User GetByUserName(string email)
        {
            return this.context.Users.Where(u => u.UserName == email).Select(u => u).FirstOrDefault();
        }

        public Client GetClientByUserName(string email)
        {
            return this.context.Clients.Where(c => c.User.UserName == email).Select(c => c).FirstOrDefault();
        }

        public IQueryable GetClientsInclude()
        {
            return this.context.Clients.Include(c => c.State).Include(c => c.SubscriptionType).Include(c => c.User).Include(c => c.ZipCode);
        }


        public Task<Client> GetByIdAsyncInclude(int id)
        {
            return this.context.Clients.Where(i => i.Id == id).Include(c => c.State).Include(c => c.SubscriptionType).Include(c => c.User).Include(c => c.ZipCode).FirstOrDefaultAsync();
        }

        public List<Client> GetClientsList()
        {
            return this.context.Clients.ToList();
        }

        public string GetClientState(int id)
        {
            return this.context.States.Where(s => s.Id == id).Select(s => s.Name).FirstOrDefault();
        }

        public string GetClientStateByUserName(string username)
        {
            var id = this.context.Clients.Where(c => c.Email == username).Select(c => c.StateId).FirstOrDefault();

            return this.context.States.Where(s => s.Id == id).Select(s => s.Name).FirstOrDefault();
        }

        public string GetClientSubscryptionType(int id)
        {
            return this.context.SubscriptionType.Where(s => s.Id == id).Select(s => s.Name).FirstOrDefault();
        }

        public string GetClientZipCode(int id)
        {
            return this.context.ZipCode.Where(s => s.Id == id).Select(s => s.Name).FirstOrDefault();
        }

        public string GetClientFirstLetter(string name)
        {
            string letter = name.Substring(0, 1).ToUpper();

            return letter;
        }

        public void OnlyUpdateClientState(ChangeStateViewModel state)
        {
            var clientToChange = this.context.Clients.Where(c => c.Email == state.UserName).SingleOrDefault();
            var stateId = this.context.States.Where(c => c.Id == int.Parse(state.StateId)).Select(c => c.Id).SingleOrDefault();
            clientToChange.StateId = stateId;
            context.Entry(clientToChange).Property("StateId").IsModified = true;
            context.SaveChanges();
        }

        public void OnlyUpdateViewerEmailConfirmedToFalse(User user)
        {
            var userToChange = this.context.Users.Where(c => c.Email == user.UserName).SingleOrDefault();
            user.EmailConfirmed = false;
            context.Entry(userToChange).Property("EmailConfirmed").IsModified = true;
            context.SaveChanges();
        }

        public void OnlyUpdateViewerEmailConfirmedToTrue(User user)
        {
            var userToChange = this.context.Users.Where(c => c.Email == user.UserName).SingleOrDefault();
            user.EmailConfirmed = true;
            context.Entry(userToChange).Property("EmailConfirmed").IsModified = true;
            context.SaveChanges();
        }
    }
}
