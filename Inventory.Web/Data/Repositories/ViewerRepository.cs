﻿namespace Inventory.Web.Data.Repositories
{
    using Inventory.Web.Data.Entities;
    using Inventory.Web.Data.IRepositories;
    using Microsoft.EntityFrameworkCore;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class ViewerRepository : GenericRepository<Viewer>, IViewerRepository
    {
        private readonly DataContext context;

        public ViewerRepository(DataContext context) : base(context)
        {
            this.context = context;
        }

        public Viewer GetViewerByUserName(string email)
        {
            return this.context.Viewers.Where(c => c.Email == email).FirstOrDefault();
        }

        public IQueryable GetViewersWithClient()
        {
            return this.context.Viewers.Include(i => i.Client);
        }

        public IQueryable GetThisClientViewers(string email)
        {
            return this.context.Viewers.Include(i => i.Client).Where(c => c.Client.Email == email);
        }

        public Task<Viewer> GetByIdAsyncInclude(int id)
        {
            return this.context.Viewer.Where(i => i.Id == id).Include(a => a.Client)
                .FirstOrDefaultAsync();
        }

        //****************** Viewer Inventories ***********************

        public IQueryable GetViewerInventories()
        {
            return this.context.ViewerInventories.Include(i => i.Inventory).Include(i => i.Viewer);
        }


        public IQueryable GetThisClientViewerInventories(string email)
        {
            return this.context.ViewerInventories.Include(i => i.Inventory).Include(i => i.Viewer).Where(c => c.Viewer.Client.Email ==email);
        }

        public Task<ViewerInventories> GetViewerInventoriesById(int id)
        {
            return  this.context.ViewerInventories.Include(i => i.Inventory).Include(i => i.Viewer).Where(i => i.Id == id).FirstOrDefaultAsync();
        }





        public async Task<ViewerInventories> CreateViewerInventoryAsync(ViewerInventories viewerInventories)
        {
            await this.context.ViewerInventories.AddAsync(viewerInventories);
            await SaveAllOthersAsync();
            return viewerInventories;
        }



        public async Task<ViewerInventories> UpdateViewerInventoriesAsync(ViewerInventories viewerInventories)
        {
            this.context.ViewerInventories.Update(viewerInventories);
            await SaveAllOthersAsync();
            return viewerInventories;
        }


        public async Task DeleteViewerInventoriesAsync(ViewerInventories viewerInventories)
        {
            this.context.ViewerInventories.Remove(viewerInventories);
            await SaveAllOthersAsync();

        }


        public async Task<bool> ViewerInventoriesExistsAsync(int id)
        {
            return await this.context.ViewerInventories.AnyAsync(e => e.Id == id);
        }

        public async Task<bool> SaveAllOthersAsync()
        {
            return await this.context.SaveChangesAsync() > 0;
        }

        public List<int> GetIdViewersList(int id)
        {
            return this.context.ViewerInventories.Where(v => v.AInventoryId == id).Select(v => v.ViewerId).ToList();
        }

        public List<Viewer> GetViewersList(List<int> idList)
        {
            List<Viewer> ViewersList = new List<Viewer>();

            foreach (var item in idList)
            {
                ViewersList.Add(this.context.Viewer.Where(v => v.Id == item).Select(v => v).FirstOrDefault());
            }

            return ViewersList;
        }

        public List<int> GetIdViewersListClient(int id)
        {
            return this.context.Viewers.Where(v => v.ClientId == id).Select(v => v.Id).ToList();
        }

        public List<int> GetIdViewersListNotInInventory(int id, string username)
        {
            List<int> listToSend = new List<int>();

            List<ViewerInventories> list = new List<ViewerInventories>();

            var user = this.context.Clients.Where(c => c.Email == username).FirstOrDefault();

            var viewers = this.context.Viewers.Where(v => v.ClientId == user.Id).ToList();

            if (viewers.Count() != 0)
            {
                foreach (var item in viewers)
                {
                    var toAdd = this.context.ViewerInventories.Where(v => v.ViewerId == item.Id).FirstOrDefault();

                    if (toAdd != null)
                    {
                        list.Add(toAdd);
                    }
                    else
                    {
                        listToSend.Add(item.Id);
                    }
                }
            }

            if (list.Count() != 0)
            {
                foreach (var item in list)
                {
                    if (item.AInventoryId != id)
                    {
                        listToSend.Add(item.Id);
                    }
                }
            }

            return listToSend;
        }

        public List<int> GetIdViewersListForInventories(string username)
        {
            var viewer = this.context.Viewers.Where(v => v.Email == username).Select(v => v.Id).FirstOrDefault();

            var list = this.context.ViewerInventories.Where(v => v.ViewerId == viewer).Select(v => v.AInventoryId).ToList();

            return list;
        }

        public async Task<ViewerInventories> GetViewerInventoryByInventoryAndViewer(int id, int idViewer)
        {
            return await this.context.ViewerInventories.Where(v => v.AInventoryId == id).Where(v => v.ViewerId == idViewer).FirstOrDefaultAsync();
        }

        public List<Viewer> GetThisClientViewersList(string email)
        {
            var viewer = this.context.Clients.Where(c => c.Email == email).Select(c => c.Id).FirstOrDefault();

            var list = this.context.Viewers.Where(v => v.ClientId == viewer).ToList();

            return list;
        }

        public List<ViewerInventories> GetViewerInventoriesList(List<int> idList)
        {
            List<ViewerInventories> ViewersList = new List<ViewerInventories>();

            foreach (var item in idList)
            {
                ViewersList.Add(this.context.ViewerInventories.Where(v => v.Id == item).Select(v => v).FirstOrDefault());
            }

            return ViewersList;
        }

        public List<ViewerInventories> GetViewerInventoriesListByInventory(int id)
        {
            return this.context.ViewerInventories.Where(v => v.AInventoryId == id).ToList();
        }
    }
}
