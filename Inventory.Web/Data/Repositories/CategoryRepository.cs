﻿

namespace Inventory.Web.Data.Repositories
{
    using Inventory.Web.Data.Entities;
    using Inventory.Web.Data.IRepositories;
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        private readonly DataContext context;

        public CategoryRepository(DataContext context) : base(context)
        {
            this.context = context;
        }
    }
}
