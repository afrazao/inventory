﻿


namespace Inventory.Web.Data.Repositories
{
    using Inventory.Web.Data.Entities;
    using Inventory.Web.Data.IRepositories;
    using System.Linq;
    using System.Threading.Tasks;

    public class ZipCodeRepository : GenericRepository<ZipCode>, IZipCodeRepository
    {
        private readonly DataContext context;

        public ZipCodeRepository(DataContext context) : base(context)
        {
            this.context = context;
        }

        public  ZipCode GetZipCodeByAddress(string address)
        {
            return  this.context.ZipCode.Where(i => i.Name== address).FirstOrDefault();
        }

        public ZipCode GetZipCodeByCode(string code)
        {
            return this.context.ZipCode.Where(i => i.Code == code).FirstOrDefault();
        }
    }
}
