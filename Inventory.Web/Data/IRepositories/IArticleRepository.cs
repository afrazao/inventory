﻿

namespace Inventory.Web.Data.IRepositories
{
    using Inventory.Web.Data.Entities;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public interface IArticleRepository : IGenericRepository<Article>
    {
        IQueryable GetAllInclude();

        Task<Article> GetByIdAsyncInclude(int id);

        List<Article> GetArticles();

        Article FindArticleByCode(string code);

        int GetArticleId(string barcode);
        List<Article> GetArticlesByInventoryArticlesId(System.Collections.Generic.List<int> idList);
        List<Article> GetArticlesByCategory(List<Article> list, int categoryid);
        string GetArticleAddressById(int articleId);
    }
}
