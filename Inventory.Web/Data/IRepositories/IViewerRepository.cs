﻿namespace Inventory.Web.Data.IRepositories
{
    using Inventory.Web.Data.Entities;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public interface IViewerRepository : IGenericRepository<Viewer>
    {
        IQueryable GetViewersWithClient();


        IQueryable GetThisClientViewers(string email);

        Task<Viewer> GetByIdAsyncInclude(int id);

        //************* Viewer Inventories *********************


        IQueryable GetViewerInventories();


        IQueryable GetThisClientViewerInventories(string email);

        Task<ViewerInventories> GetViewerInventoriesById(int id);


        Task<ViewerInventories> CreateViewerInventoryAsync(ViewerInventories viewerInventories);




        Task<ViewerInventories> UpdateViewerInventoriesAsync(ViewerInventories viewerInventories);




        Task DeleteViewerInventoriesAsync(ViewerInventories viewerInventories);



        Task<bool> ViewerInventoriesExistsAsync(int id);


        Task<bool> SaveAllOthersAsync();

        List<int> GetIdViewersList(int id);

        List<Viewer> GetViewersList(List<int> idList);

        List<int> GetIdViewersListClient(int id);
       
        List<int> GetIdViewersListNotInInventory(int id, string username);
        List<int> GetIdViewersListForInventories(string username);
        Task<ViewerInventories> GetViewerInventoryByInventoryAndViewer(int id, int idViewer);
        List<Viewer> GetThisClientViewersList(string email);
        Viewer GetViewerByUserName(string email);
        List<ViewerInventories> GetViewerInventoriesList(List<int> idList);
        List<ViewerInventories> GetViewerInventoriesListByInventory(int id);
    }
}
