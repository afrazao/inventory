﻿
namespace Inventory.Web.Data.IRepositories
{
    using Inventory.Web.Data.Entities;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public interface IAInventoryRepository : IGenericRepository<AInventory>
    {
        IQueryable GetAllWithClients();

        IQueryable GetThisClientInventories(string email);

        Task<AInventory> GetByIdAsyncInclude(int id);

        List<AInventory> GetInventoryListById(int id);

        List<AInventory> GetInventoriesListByClient(string email);


        //************ InventoryArticles ************************

        IQueryable GetThisInventoryInventoryArticles(int inventoryId);

        InventoryArticle GetInventoryArticleByName(string name);


        string GetInventoryArticleName(int id);



        IQueryable GetInventoryArticles();


        Task<InventoryArticle> GetInventoryArticleById(int id);



        Task<InventoryArticle> CreateInventoryArticleAsync(InventoryArticle inventoryArticle);


        Task<InventoryArticle> UpdateInventoryArticleAsync(InventoryArticle inventoryArticle);


        Task DeleteInventoryArticleAsync(InventoryArticle inventoryArticle);



        Task<bool> InventoryArticleExistsAsync(int id);

        List<InventoryArticle> GetInventoryArticles(int id);

        //************ States ******************

        string GetStateName(int id);

        IQueryable GetStates();

        Task<State> GetStateById(int id);

        Task<State> CreateStateAsync(State state);

        Task<State> UpdateStateAsync(State state);

        Task DeleteStateAsync(State state);

        Task<bool> StateExistsAsync(int id);

        State GetStateByName(string name);


        //********* Inventory types *********************
        IQueryable GetInventoryTypes();

        Task<InventoryType> GetInventoryTypeById(int id);

        Task<InventoryType> CreateInventoryTypeAsync(InventoryType inventoryType);

        Task<bool> SaveAllOthersAsync();

        Task<InventoryType> UpdateInventoryTypeAsync(InventoryType inventoryType);

        Task DeleteInventoryTypeAsync(InventoryType inventoryType);

        Task<bool> InventoryTypeExistsAsync(int id);
        InventoryArticle GetInventoryArticleByArticleId(int id);
        List<AInventory> GetInventoriesByViewer(List<int> list);
        string InventoryName(int inventoryId);
        void OnlyUpdateInventoryState(int id, int stateId);
        int GetStateIdByName(string name);
    }
}
