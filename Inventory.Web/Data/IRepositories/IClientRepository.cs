﻿

namespace Inventory.Web.Data.IRepositories
{
    using Inventory.Web.Data.Entities;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public interface IClientRepository : IGenericRepository<Client>
    {
        User GetByUserName(string email);

        Client GetClientByUserName(string email);

        IQueryable GetClientsInclude();

        Task<Client> GetByIdAsyncInclude(int id);

        List<Client> GetClientsList();
        string GetClientState(int id);
        string GetClientFirstLetter(string name);
        string GetClientSubscryptionType(int id);
        string GetClientZipCode(int id);
        void OnlyUpdateClientState(Models.ChangeStateViewModel state);
        void OnlyUpdateViewerEmailConfirmedToFalse(User user);
        void OnlyUpdateViewerEmailConfirmedToTrue(User user);
        string GetClientStateByUserName(string username);
    }
}
