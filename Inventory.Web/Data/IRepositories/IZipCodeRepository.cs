﻿

namespace Inventory.Web.Data.IRepositories
{
    using Inventory.Web.Data.Entities;
    using System.Linq;
    using System.Threading.Tasks;

    public interface IZipCodeRepository : IGenericRepository<ZipCode>
    {

        ZipCode GetZipCodeByAddress(string address);

        ZipCode GetZipCodeByCode(string code);
    }
}
