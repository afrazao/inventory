﻿

namespace Inventory.Web.Data.IRepositories
{
    using Inventory.Web.Data.Entities;
    public interface ISubscriptionTypeRepository : IGenericRepository<SubscriptionType>
    {
    }
}
