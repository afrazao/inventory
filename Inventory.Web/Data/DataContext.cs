﻿using Inventory.Web.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Data
{
    public class DataContext :IdentityDbContext<User>
    {
        public DbSet<Viewer> Viewers { get; set; }

        public DbSet<ViewerInventories> ViewerInventories { get; set; }
        public DbSet<Client> Clients {get;set;}

        public DbSet<InventoryType> InventoryTypes { get; set; }

        public DbSet<AInventory> Inventories { get; set; }

        public DbSet<Article> Articles { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<InventoryArticle> InventoryArticles { get; set; }

        public DbSet<State> States { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var cascadeFKs = modelBuilder.Model.GetEntityTypes().SelectMany(t => t.GetForeignKeys()).Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach(var fk in cascadeFKs)
            {
                fk.DeleteBehavior = DeleteBehavior.Restrict;
            }

            base.OnModelCreating(modelBuilder);
        }





        public DbSet<Inventory.Web.Data.Entities.SubscriptionType> SubscriptionType { get; set; }





        public DbSet<Inventory.Web.Data.Entities.ZipCode> ZipCode { get; set; }





        public DbSet<Inventory.Web.Data.Entities.Viewer> Viewer { get; set; }





    }
}
