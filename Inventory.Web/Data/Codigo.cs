﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Data
{
    public class Codigo
    {
       
            public string arteria { get; set; }
            public string localidade { get; set; }
            public string troco { get; set; }
            public string zona { get; set; }
            public string cp7 { get; set; }
        
    }
}
