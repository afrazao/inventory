﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Data
{
    public class CodigoComGps
    {
        public string latitude { get; set; }
        public string cp4 { get; set; }
        public string longitude { get; set; }
    }
}
