﻿using Inventory.Web.Data.Entities;
using Inventory.Web.Helpers;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Web.Data
{
    public class SeedDB
    {

        private readonly DataContext context;
        private readonly IUserHelper userHelper;
        private Random random;

        public SeedDB(DataContext context, IUserHelper userHelper)
        {
            this.context = context;
            this.userHelper = userHelper;
            this.random = new Random();
        }

        public async Task SeedAsync()
        {
            await this.context.Database.EnsureCreatedAsync();

            await this.userHelper.CheckRoleAsync("Admin");
            await this.userHelper.CheckRoleAsync("InventoryAdmin");
            await this.userHelper.CheckRoleAsync("InventoryCRU");
            await this.userHelper.CheckRoleAsync("InventoryView");

            var user = await this.userHelper.GetUserByEmailAsync("jarinventory@gmail.com");
            if(user == null)
            {
                user = new User
                {
                    FirstName = "Admin",
                    LastName = "JAR",
                    Email = "jarinventory@gmail.com",
                    UserName = "jarinventory@gmail.com",
                  
                    EmailConfirmed = true
                };

                var result = await this.userHelper.AddUserAsync(user, "Senha123!");
                if (result != IdentityResult.Success)
                {
                    throw new InvalidOperationException("Could not create the user in seeder");
                }

                await this.userHelper.AddUserToRoleAsync(user, "Admin"); //Atribui quando cria o User
            }

            var isRole = await this.userHelper.IsUserInRoleAsync(user, "Admin"); //Caso o User já tenha sido criado verifica se tem o Role associado

            if (!isRole)
            {
                await this.userHelper.AddUserToRoleAsync(user, "Admin");
            }

            //se nao existir nada na tabela
            if(!this.context.States.Any())
            {
                this.AddState("Opened");
                this.AddState("Active");
                this.AddState("Idle");
                this.AddState("Cancelled");

                //this.AddState("Active");
                //this.AddState("Inactive");

                await this.context.SaveChangesAsync();
            }

            if (!this.context.Categories.Any())
            {
                this.AddCategory("Immobilized");
                this.AddCategory("Consumable");
                this.AddCategory("Perishable");
                this.AddCategory("Dangerous");

                await this.context.SaveChangesAsync();
            }

            if (!this.context.InventoryTypes.Any())
            {
                this.AddInventoryType("Library");
                this.AddInventoryType("Store");
                this.AddInventoryType("Miscelaneous");
                this.AddInventoryType("Warehouse");

                await this.context.SaveChangesAsync();
            }

            if (!this.context.SubscriptionType.Any())
            {
                this.AddSubscriptionType("Premium", 100, "5 GB", "24/7");
                this.AddSubscriptionType("Standard", 60, "5 GB", "24/7 WeekDays");
                this.AddSubscriptionType("Ultimate", 150, "20 GB", "24/7");

                await this.context.SaveChangesAsync();
            }

         


        }

        private void AddInventoryType(string name)
        {
            this.context.InventoryTypes.Add(new InventoryType
            {
                Name = name,
            });
        }

      

        private void AddCategory(string name)
        {
            this.context.Categories.Add(new Category
            {
                Name = name,
            });
        }

        private void AddState(string name)
        {
            this.context.States.Add(new State
            {
                Name = name,
            });
        }

        private void AddSubscriptionType(string name, int price, string space, string service)
        {
            this.context.SubscriptionType.Add(new SubscriptionType
            {
                Name = name,
                Price = price,
                Space = space,
                CustomerService = service
            });
        }







    }
}
