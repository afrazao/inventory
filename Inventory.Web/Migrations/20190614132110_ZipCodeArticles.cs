﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Inventory.Web.Migrations
{
    public partial class ZipCodeArticles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Articles",
                maxLength: 100,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "Articles");
        }
    }
}
