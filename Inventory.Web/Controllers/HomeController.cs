﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Inventory.Web.Models;
using Inventory.Web.Data.IRepositories;
using Inventory.Web.Data.Entities;
using Microsoft.AspNetCore.Authorization;

namespace Inventory.Web.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private readonly ISubscriptionTypeRepository subscriptionTypeRepository;

        public HomeController(ISubscriptionTypeRepository subscriptionTypeRepository)
        {
            this.subscriptionTypeRepository = subscriptionTypeRepository;
        }

        public IActionResult Index()
        {
            List<SubscriptionType> subscriptionTypes = new List<SubscriptionType>();
            subscriptionTypes = this.subscriptionTypeRepository.GetAll().OrderBy(p =>p.Price).ToList();
            ViewBag.subscriptions = subscriptionTypes;
            return View("IndexSas"); //Home
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
