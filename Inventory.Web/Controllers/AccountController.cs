﻿using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;
using Inventory.Web.Helpers;
using Inventory.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserHelper userHelper;
        private readonly IConfiguration configuration;
        private readonly IClientRepository clientRepository;
        private readonly IViewerRepository viewerRepository;
        private readonly EmailSender emailSender;

        public AccountController(IUserHelper userHelper, IConfiguration configuration, IClientRepository clientRepository, IViewerRepository viewerRepository)
        {
            this.userHelper = userHelper;
            this.configuration = configuration;
            this.clientRepository = clientRepository;
            this.viewerRepository = viewerRepository;
            this.emailSender = new EmailSender();
        }

        [AllowAnonymous]
        public IActionResult Login()
        {
            if (this.User.Identity.IsAuthenticated)
            {
                return this.RedirectToAction("Index", "Home");
            }

            return this.View();
        }

        //TODO : InventoriesRepository add GetUserByEmailAsync(this.User.Identity.Name)
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {

                var user = await userHelper.GetUserByEmailAsync(model.UserName);

                if(user == null)
                {
                    ModelState.AddModelError(string.Empty,"This email or password are incorrect");
                    return View();
                    
                }

                if (!await this.userHelper.IsEmailConfirmedAsync(await this.userHelper.GetUserByEmailAsync(model.UserName)))
                {
                    var client = this.clientRepository.GetClientByUserName(model.UserName);
                    var viewer = this.viewerRepository.GetViewerByUserName(model.UserName);
                    if(client == null && viewer == null && model.UserName != "jarinventory@gmail.com")
                    {
                        return View("EmailNotConfirmed");
                    }
                    else
                    {
                        return View("EmailNotConfirmedForClientViewer");
                    }
                }
                var result = await this.userHelper.LoginAsync(model);
                if (result.Succeeded)
                {
                 

                    if (this.Request.Query.Keys.Contains("ReturnUrl"))
                    {
                        return this.Redirect(this.Request.Query["ReturnUrl"].First());
                    }


                  
                    var client = this.clientRepository.GetClientByUserName(model.UserName);
                    var viewer = this.viewerRepository.GetViewerByUserName(model.UserName);
                    if(client==null && viewer == null && model.UserName!= "jarinventory@gmail.com")
                    {
                        return this.RedirectToAction("Create", "Clients");
                    }

                    return this.RedirectToAction("Index", "Home");
                }
            }

            this.ModelState.AddModelError(string.Empty, "Failed to Login");
            return this.View(model);
        }

        public async Task<IActionResult> Logout()
        {
            await this.userHelper.LogoutAsync();
            return this.RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public IActionResult Register()
        {
            return this.View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterNewUserViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = await this.userHelper.GetUserByEmailAsync(model.UserName);
                if(user == null)
                {
                    user = new User
                    {
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Email = model.UserName,
                        UserName = model.UserName,
                        EmailConfirmed = false
                    };

                var result = await this.userHelper.AddUserAsync(user, model.Password);
                if(result != IdentityResult.Success)
                {
                    this.ModelState.AddModelError(string.Empty, "The user could not be created.");
                    return this.View(model);
                }

                if (result.Succeeded)
                    {
                        await this.userHelper.AddUserToRoleAsync(user, "InventoryAdmin");
                        string ctoken = userHelper.GetGenerateEmailConfirmationTokenAsync(user).Result;
                        string ctokenlink = Url.Action("ConfirmEmail", "Account", new
                        {
                            userid = user.Id,
                            token = ctoken
                        }, protocol: HttpContext.Request.Scheme);

                        await emailSender.SendEmailAsync(
                            "jarinventory@gmail.com",
                            "Your Inventory Now Account Confirmation",
                                            $"The User {user.Email} is waiting for a confirmation. To confirm click in the link: {ctokenlink}");
                        //                    $"" +
                        //$"<p style='font-size: 16px; line-height: 1.5em; margin-top: 0;box-sizing: border-box;font-family: Helvetica, Arial, sans-serif;font-size: 18px;'>Hello there,</p>" +
                        //$"<p style='font-size: 16px; line-height: 1.5em; margin-top: 0;box-sizing: border-box;font-family: Helvetica, Arial, sans-serif;font-size: 15px;'>{user.FirstName + " " + user.LastName} has recently signed up and waiting for your approve. </br> Please click in below link to confirm client account</p> <br>" +
                        //$"<a style='padding: 8px 12px; border: 2px solid #ED2939;border-radius: 18px;font-family: Helvetica, Arial, sans-serif;text-decoration:none;font-size: 14px;background-color:#ED2939; color: #ffffff;font-weight:bold;display:inline-block;' href=\"" + ctokenlink + "\">Click here</a>");

                        return RedirectToAction("AwaitEmailConfirmation");
                    }


                    //ViewData["TokenLink"] = ctokenlink;

                    //var loginViewModel = new LoginViewModel
                    //{
                    //    Password = model.Password,
                    //    RememberMe = false,
                    //    UserName = model.UserName
                    //};

                    //var resultSignIn = await this.userHelper.LoginAsync(loginViewModel);

                    //if (resultSignIn.Succeeded)
                    //{
                    //    return this.RedirectToAction("Create", "Clients");
                    //}

                    this.ModelState.AddModelError(string.Empty, "The User already exists");
                    return this.View(model);
                }

                this.ModelState.AddModelError(string.Empty, "The username is already registered.");
            }
            
        return this.View(model);
        }

        [Authorize]
        public async Task<IActionResult> ChangeUser()
        {
            var user = await this.userHelper.GetUserByEmailAsync(this.User.Identity.Name);
            var model = new ChangeUserViewModel();

            if(user != null)
            {
                model.FirstName = user.FirstName;
                model.LastName = user.LastName;
            }

            return this.View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangeUser(ChangeUserViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = await this.userHelper.GetUserByEmailAsync(this.User.Identity.Name);
                if(user!= null)
                {
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    var response = await this.userHelper.UpdateUserAsync(user);
                    if (response.Succeeded)
                    {
                        this.ViewBag.UserMessage = "User Updated";
                    }
                    else
                    {
                        this.ModelState.AddModelError(string.Empty, response.Errors.FirstOrDefault().Description);
                    }
                }
            }
            else
            {
                this.ModelState.AddModelError(string.Empty, "User not found.");
            }

            return this.View(model);
        }

        [AllowAnonymous]
        public IActionResult NewPassword()
        {
            return View();
        }

        //GET: /Account/AwaitPasswordChangeConfirmation
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> AwaitPasswordChangeConfirmation(ResetPasswordViewModel model)
        {
            if (model.UserName == null)
            {
                ModelState.AddModelError(string.Empty, "You have to input an email");
                return View();
            }

            var emailClient = this.userHelper.GetUserByEmail(model.UserName);

            if(emailClient == null)
            {
                ModelState.AddModelError(string.Empty, "You have to input an email that already exists");
                return View();
            }

            var user = await this.userHelper.GetUserByEmailAsync(model.UserName);
            //if (user == null)
            //{
            //    user = new User
            //    {
            //        Email = model.UserName,
            //    };
            //}
            string ctoken = userHelper.GetGeneratePasswordResetTokenAsync(user).Result;
            string ctokenlink = Url.Action("ResetPassword", "Account", new
            {
                userid = user.Id,
                token = ctoken
            }, protocol: HttpContext.Request.Scheme);

            await emailSender.SendEmailAsync(
                user.Email,
                "Your Inventory Now Account Password Reset",
                $"Please click on the link to reset your password: {ctokenlink}");
                //$"" +
                //        $"<p style='font-size: 16px; line-height: 1.5em; margin-top: 0;box-sizing: border-box;font-family: Helvetica, Arial, sans-serif;font-size: 18px;'>Hello there,</p>" +
                //        $"<p style='font-size: 16px; line-height: 1.5em; margin-top: 0;box-sizing: border-box;font-family: Helvetica, Arial, sans-serif;font-size: 15px;'>{user.FirstName + " " + user.LastName} has recently signed up and waiting for your approve. </br> Please click in below link to confirm client account</p> <br>" +
                //        $"<a style='padding: 8px 12px; border: 2px solid #ED2939;border-radius: 18px;font-family: Helvetica, Arial, sans-serif;text-decoration:none;font-size: 14px;background-color:#ED2939; color: #ffffff;font-weight:bold;display:inline-block;' href=\"" + ctokenlink + "\">Click here</a>");

            //user.LockoutEnabled = true;

            return View();
        }

        [Authorize]
        public IActionResult ChangePassword()
        {
            return this.View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = await this.userHelper.GetUserByEmailAsync(this.User.Identity.Name);
                if (user != null)
                {
                    var result = await this.userHelper.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return this.RedirectToAction("ChangeUser");
                    }
                    else
                    {
                        this.ModelState.AddModelError(string.Empty, result.Errors.FirstOrDefault().Description);
                    }
                }
                else
                {
                    this.ModelState.AddModelError(string.Empty, "User not found.");
                }
            }

            return this.View(model);
        }

        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(string userId, string token)
        {
            if (userId == null || token == null)
            {
                return View("Error");
            }
            var user = await userHelper.GetUserByIdAsync(userId);
            if (user == null)
            {
                return View("Error");
            }

            ViewData["User"] = user.UserName;

            return this.View();
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword(string userId, NewPasswordViewModel model, string token)
        {

            if (this.ModelState.IsValid)
            {
                var user = await this.userHelper.GetUserByIdAsync(userId);
                if (user != null)
                {
                    //var result = await this.userHelper.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    var result = await this.userHelper.ResetPasswordAsync(user, token, model.NewPassword);
                    if (result.Succeeded)
                    {
                        //user.LockoutEnabled = false;
                        return this.RedirectToAction("Login");
                    }
                    else
                    {
                        this.ModelState.AddModelError(string.Empty, result.Errors.FirstOrDefault().Description);
                    }
                }
                else
                {
                    this.ModelState.AddModelError(string.Empty, "User not Found");
                }
            }

            return this.View(model);
        }


        //TODO: VÍDEO 33 27:55
        [HttpPost]
        public async Task<IActionResult> CreateToken([FromBody] LoginViewModel model)
        {
            if (this.ModelState.IsValid)
            {
                var user = await this.userHelper.GetUserByEmailAsync(model.UserName);
                if(user != null)
                {
                    var result = await this.userHelper.ValidatePasswordAsync(user, model.Password);

                    if (result.Succeeded)
                    {
                        var claims = new[]
                        {
                            new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                        };

                        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.configuration["Tokens:Key"]));
                        var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                        var token = new JwtSecurityToken(
                            this.configuration["Tokens:Issuer"],
                            this.configuration["Tokens:Audience"],
                            claims,
                            expires: DateTime.UtcNow.AddMonths(3),
                            signingCredentials: credentials);

                        var results = new
                        {
                            token = new JwtSecurityTokenHandler().WriteToken(token),
                            expiration = token.ValidTo
                        };

                        return this.Created(string.Empty, results);
                    }
                    else
                    {
                        this.ModelState.AddModelError(string.Empty, "Password is not valid.");
                    }
                }
                else
                {
                    this.ModelState.AddModelError(string.Empty, "User not Found");
                }
            }
            return this.BadRequest();
        }

        //GET: /Account/ConfirmEmail
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            if (userId == null || token == null)
            {
                return View("Error");
            }
            var user = await userHelper.GetUserByIdAsync(userId);
            if (user == null)
            {
                return View("Error");
            }

            user.EmailConfirmed = true;

            var result = await userHelper.GetConfirmEmailAsync(user, token);

            string userlink = Url.Action("Login", "Account", null, protocol: HttpContext.Request.Scheme);

            await emailSender.SendEmailAsync(
            user.Email,
            "Your Inventory Now Account Confirmation",
            $"Your account was confirmed by our Team. To sign in, follow this link: {userlink}");

            ViewData["User"] = user.UserName;

            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //GET: /Account/AwaitEmailConfirmation
        [HttpGet]
        [AllowAnonymous]
        public IActionResult AwaitEmailConfirmation()
        {
            return View();
        }

        //GET: /Account/EmailNotConfirmed
        [HttpGet]
        [AllowAnonymous]
        public IActionResult EmailNotConfirmed()
        {
            return View();
        }
    }
}
