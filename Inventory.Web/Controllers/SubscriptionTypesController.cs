﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;
using Microsoft.AspNetCore.Authorization;

namespace Inventory.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SubscriptionTypesController : Controller
    {
       
        private readonly ISubscriptionTypeRepository subscriptionTypeRepository;

        public SubscriptionTypesController(ISubscriptionTypeRepository subscriptionTypeRepository)
        {
          
            this.subscriptionTypeRepository = subscriptionTypeRepository;
        }

        // GET: SubscriptionTypes
        public IActionResult Index()
        {
            return View(this.subscriptionTypeRepository.GetAll());
        }

        // GET: SubscriptionTypes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var subscriptionType = await this.subscriptionTypeRepository.GetByIdAsync(id.Value);
            if (subscriptionType == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(subscriptionType);
        }

        // GET: SubscriptionTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SubscriptionTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,Price,Space,CustomerService")] SubscriptionType subscriptionType)
        {
            if (ModelState.IsValid)
            {
                await this.subscriptionTypeRepository.CreateAsync(subscriptionType);
                return RedirectToAction(nameof(Index));
            }
            return View(subscriptionType);
        }

        // GET: SubscriptionTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var subscriptionType = await this.subscriptionTypeRepository.GetByIdAsync(id.Value);
            if (subscriptionType == null)
            {
                return RedirectToAction("Error404", "Error");
            }
            return View(subscriptionType);
        }

        // POST: SubscriptionTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Price,Space,CustomerService")] SubscriptionType subscriptionType)
        {
            if (id != subscriptionType.Id)
            {
                return RedirectToAction("Error404", "Error");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await this.subscriptionTypeRepository.UpdateAsync(subscriptionType);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await this.subscriptionTypeRepository.ExistsAsync(subscriptionType.Id))
                    {
                        return RedirectToAction("Error404", "Error");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(subscriptionType);
        }

        // GET: SubscriptionTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var subscriptionType = await this.subscriptionTypeRepository.GetByIdAsync(id.Value);
            if (subscriptionType == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(subscriptionType);
        }

        // POST: SubscriptionTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var subscriptionType = await this.subscriptionTypeRepository.GetByIdAsync(id);
            try
            {
                await this.subscriptionTypeRepository.DeleteAsync(subscriptionType);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }
            return RedirectToAction(nameof(Index));
        }

      
    }
}
