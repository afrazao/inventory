﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;
using Microsoft.AspNetCore.Authorization;

namespace Inventory.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ZipCodesController : Controller
    {
       
        private readonly IZipCodeRepository zipCodeRepository;

        public ZipCodesController(IZipCodeRepository zipCodeRepository)
        {
           
            this.zipCodeRepository = zipCodeRepository;
        }

        // GET: ZipCodes
        public IActionResult Index()
        {
            return View(this.zipCodeRepository.GetAll());
        }

        // GET: ZipCodes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var zipCode = await this.zipCodeRepository.GetByIdAsync(id.Value);
            if (zipCode == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(zipCode);
        }

        // GET: ZipCodes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ZipCodes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Code,City,Country,Name")] ZipCode zipCode)
        {
            if (ModelState.IsValid)
            {

                var codeJoin = zipCode.Code.Split('-');
                var codeConcat = codeJoin[0] + codeJoin[1];
                var anotherResponse = await Services.ApiService.GetCitiesGPS("http://codigospostais.appspot.com", $"/cp4?codigo={codeJoin[0]}");

                var codeGps = new CodigoComGps();
                codeGps = (CodigoComGps)anotherResponse.Result;

                zipCode.latitude = codeGps.latitude;
                zipCode.longitude = codeGps.longitude;

                await this.zipCodeRepository.CreateAsync(zipCode);
                return RedirectToAction(nameof(Index));
            }
            return View(zipCode);
        }

        // GET: ZipCodes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var zipCode = await this.zipCodeRepository.GetByIdAsync(id.Value);
            if (zipCode == null)
            {
                return RedirectToAction("Error404", "Error");
            }
            return View(zipCode);
        }

        // POST: ZipCodes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Code,City,Country,Name")] ZipCode zipCode)
        {
            if (id != zipCode.Id)
            {
                return RedirectToAction("Error404", "Error");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var codeJoin = zipCode.Code.Split('-');
                    var codeConcat = codeJoin[0] + codeJoin[1];
                    var anotherResponse = await Services.ApiService.GetCitiesGPS("http://codigospostais.appspot.com", $"/cp4?codigo={codeJoin[0]}");

                    var codeGps = new CodigoComGps();
                    codeGps = (CodigoComGps)anotherResponse.Result;

                    zipCode.latitude = codeGps.latitude;
                    zipCode.longitude = codeGps.longitude;
                    await this.zipCodeRepository.UpdateAsync(zipCode);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await this.zipCodeRepository.ExistsAsync(zipCode.Id))
                    {
                        return RedirectToAction("MyBadRequest", "Error");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(zipCode);
        }

        // GET: ZipCodes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var zipCode = await this.zipCodeRepository.GetByIdAsync(id.Value);
            if (zipCode == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(zipCode);
        }

        // POST: ZipCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var zipCode = await this.zipCodeRepository.GetByIdAsync(id);
            try
            {
                await this.zipCodeRepository.DeleteAsync(zipCode);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }
            return RedirectToAction(nameof(Index));
        }

        //JSON RESULT
        [HttpPost]
        public async Task<ActionResult> GetData(string Code)
        {

            var codeJoin = Code.Split('-');
            var codeConcat = codeJoin[0] + codeJoin[1];
            var response = await Services.ApiService.GetCities("http://codigospostais.appspot.com", $"/cp7?codigo={codeConcat}");


            var cities = new Codigo();
            cities = (Codigo)response.Result;



            if (cities.arteria == null)
            {
                cities.arteria = "Please insert your street name";
            }


            

            return Ok(cities.arteria);

        }


        public async Task<ActionResult> GetAnotherData(string Code)
        {

            var codeJoin = Code.Split('-');
            var codeConcat = codeJoin[0] + codeJoin[1];
            var response = await Services.ApiService.GetCities("http://codigospostais.appspot.com", $"/cp7?codigo={codeConcat}");


            var cities = new Codigo();
            cities = (Codigo)response.Result;



            if (cities.localidade == null)
            {
                cities.localidade = "Please fill with your city name";
            }




            return Ok(cities.localidade);

        }

    }
}
