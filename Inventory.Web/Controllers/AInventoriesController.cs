﻿using ClosedXML.Excel;
using DinkToPdf;
using DinkToPdf.Contracts;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;
using Inventory.Web.Helpers;
using Inventory.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Inventory.Web.Controllers
{
    public class AInventoriesController : Controller
    {
        private readonly IAInventoryRepository aInventoryRepository;
        private readonly IUserHelper userHelper;
        private readonly IClientRepository clientRepository;
        private readonly IViewerRepository viewerRepository;

        public AInventoriesController(IAInventoryRepository aInventoryRepository,IUserHelper userHelper,IClientRepository clientRepository, IViewerRepository viewerRepository)
        {

            this.aInventoryRepository = aInventoryRepository;
            this.userHelper = userHelper;
            this.clientRepository = clientRepository;
            this.viewerRepository = viewerRepository;
        }

        [Authorize(Roles ="InventoryAdmin")]
        // GET: AInventories
        public IActionResult Index()
        {
            var user = this.userHelper.GetUserByEmail(User.Identity.Name);
            var client = this.clientRepository.GetClientByUserName(User.Identity.Name);
            var viewer = this.viewerRepository.GetViewerByUserName(User.Identity.Name);
            if (client == null && viewer == null && User.Identity.Name != "jarinventory@gmail.com")
            {
                return this.RedirectToAction("Create", "Clients");
            }

            return this.View(this.aInventoryRepository.GetThisClientInventories(User.Identity.Name));
        }

        [Authorize(Roles = "Admin")]
        // GET: AInventories
        public IActionResult IndexAdmin(string username)
        {
            ViewBag.username = username;

            return this.View(this.aInventoryRepository.GetThisClientInventories(username));
        }

        [Authorize(Roles ="InventoryAdmin, Admin, InventoryView, InventoryCRU")]
        // GET: AInventories
        public IActionResult IndexViewer(string username)
        {
            if(username != null)
            {
                var listIdsV = this.viewerRepository.GetIdViewersListForInventories(username);

                var inventoriesV = this.aInventoryRepository.GetInventoriesByViewer(listIdsV);

                return this.View(inventoriesV);
            }

            var listIds = this.viewerRepository.GetIdViewersListForInventories(User.Identity.Name);

            var inventories = this.aInventoryRepository.GetInventoriesByViewer(listIds);

            return this.View(inventories);
        }

        [NonAction]
        // GET: AInventories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var aInventory = await this.aInventoryRepository.GetByIdAsyncInclude(id.Value);

            if (aInventory == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(aInventory);
        }

        [Authorize(Roles = "InventoryAdmin")]
        // GET: AInventories/Create
        public IActionResult Create()
        {
            //ViewData["ClientId"] = new SelectList(clientRepository.GetAll(), "Id", "Name");
            ViewData["InventoryTypeId"] = new SelectList(aInventoryRepository.GetInventoryTypes(), "Id", "Name");
            //ViewData["StateId"] = new SelectList(aInventoryRepository.GetStates(), "Id", "Name");
            return View();
        }

        // POST: AInventories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("InventoryTypeId,Name,ImageFile")] InventoriesViewModel view)
        {

            if(view.ImageFile==null)
            {
                ModelState.AddModelError(string.Empty, "You must select an image file");
                ViewData["InventoryTypeId"] = new SelectList(aInventoryRepository.GetInventoryTypes(), "Id", "Name", view.InventoryTypeId);
                //ViewData["StateId"] = new SelectList(aInventoryRepository.GetStates(), "Id", "Name", view.StateId);
                return View(view);
            }

            var state = this.aInventoryRepository.GetStateByName("Opened");

            var client = this.clientRepository.GetClientByUserName(User.Identity.Name);

            view.ClientId = client.Id;
            view.State = state;

            if (ModelState.IsValid)
            {
                var path = string.Empty;
                if (view.ImageFile != null && view.ImageFile.Length > 0)
                {
                    var guid = Guid.NewGuid().ToString();
                    var file = $"{guid}.jpg";
                    path = Path.Combine(
                        Directory.GetCurrentDirectory(),
                        "wwwroot\\images\\Inventories",
                        file);


                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await view.ImageFile.CopyToAsync(stream);
                    }


                    path = $"~/images/Inventories/{file}";

                }
              




                var aInventory = this.ToInventory(view, path);




                await this.aInventoryRepository.CreateAsync(aInventory);
                return RedirectToAction(nameof(Index));
            }
            //ViewData["ClientId"] = new SelectList(clientRepository.GetAll(),"Id", "Name", view.ClientId);
            ViewData["InventoryTypeId"] = new SelectList(aInventoryRepository.GetInventoryTypes(), "Id", "Name", view.InventoryTypeId);
            //ViewData["StateId"] = new SelectList(aInventoryRepository.GetStates(), "Id", "Name", view.StateId);
            return View(view);
        }

        private AInventory ToInventory(InventoriesViewModel view, string path)
        {
            var state = this.aInventoryRepository.GetStateByName("Opened");

            var client = this.clientRepository.GetClientByUserName(User.Identity.Name);

            var clientId = client.Id;

            return new AInventory
            {
                Id = view.Id,
                ImageUrl = path,
                ClientId = clientId,
                Date = DateTime.Now,
                StateId = state.Id,
                InventoryTypeId = view.InventoryTypeId,
                Name = view.Name
            };
        }

        [NonAction]
        // GET: AInventories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var aInventory = await this.aInventoryRepository.GetByIdAsync(id.Value);
            if (aInventory == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            var view = this.ToInventoryViewModel(aInventory);
            ViewData["ClientId"] = new SelectList(clientRepository.GetAll(), "Id", "Name", view.ClientId);
            ViewData["InventoryTypeId"] = new SelectList(aInventoryRepository.GetInventoryTypes(), "Id", "Name", view.InventoryTypeId);
            ViewData["StateId"] = new SelectList(aInventoryRepository.GetStates(), "Id", "Name", view.StateId);
            return View(view);
        }

        private InventoriesViewModel ToInventoryViewModel(AInventory aInventory)
        {
            return new InventoriesViewModel
            {
                Id = aInventory.Id,
                ImageUrl = aInventory.ImageUrl,
                ClientId = aInventory.ClientId,
                Date = aInventory.Date,
                StateId = aInventory.StateId,
                InventoryTypeId = aInventory.InventoryTypeId,
                Name = aInventory.Name
            };
        }

        // POST: AInventories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ClientId,Date,StateId,InventoryTypeId,Name,ImageFile")] InventoriesViewModel view)
        {
            if (id != view.Id)
            {
                return RedirectToAction("Error404", "Error");
            }

            if (ModelState.IsValid)
            {
                try
                {

                    var path = view.ImageUrl;

                    if (view.ImageFile != null && view.ImageFile.Length > 0)
                    {
                        var guid = Guid.NewGuid().ToString();
                        var file = $"{guid}.jpg";
                        path = Path.Combine(
                            Directory.GetCurrentDirectory(),
                            "wwwroot\\images\\Inventories",
                            file);


                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await view.ImageFile.CopyToAsync(stream);
                        }

                        //TODO:falta fazer o delete das imagens quando faz delete
                        path = $"~/images/Inventories/{file}";

                    }


                    var aInventory = this.ToInventory(view, path);

                    await this.aInventoryRepository.UpdateAsync(aInventory);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await this.aInventoryRepository.ExistsAsync(view.Id))
                    {
                        return RedirectToAction("Error404", "Error");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientId"] = new SelectList(clientRepository.GetAll(), "Id", "Name", view.ClientId);
            ViewData["InventoryTypeId"] = new SelectList(aInventoryRepository.GetInventoryTypes(), "Id", "Name", view.InventoryTypeId);
            ViewData["StateId"] = new SelectList(aInventoryRepository.GetStates(), "Id", "Name", view.StateId);
            return View(view);
        }

        [Authorize(Roles = "InventoryAdmin")]
        public async Task<IActionResult> EditInventoryState(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var aInventory = await this.aInventoryRepository.GetByIdAsync(id.Value);
            if (aInventory == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            ViewBag.username = aInventory.Name;

            var userState = this.aInventoryRepository.GetStateName(aInventory.Id);

            ViewBag.state = userState;

            ViewData["StateId"] = new SelectList(this.aInventoryRepository.GetStates(), "Id", "Name");

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> EditInventoryState(EditInventoryStateViewModel view, int id)
        {
            var aInventory = await this.aInventoryRepository.GetByIdAsync(id);

            var userState = this.aInventoryRepository.GetStateName(aInventory.StateId);

            var state = this.aInventoryRepository.GetStateIdByName(userState);

            if (view.StateId != state)
            {
                this.aInventoryRepository.OnlyUpdateInventoryState(aInventory.Id, view.StateId);
            }

            if (view.StateId == 4)
            {

                var listToDelete = this.viewerRepository.GetViewerInventoriesListByInventory(aInventory.Id);

                foreach (var item in listToDelete)
                {
                    await this.viewerRepository.DeleteViewerInventoriesAsync(item);
                }
            }

            return RedirectToAction("Index", "AInventories");
        }

        [NonAction]
        // GET: AInventories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var aInventory = await this.aInventoryRepository.GetByIdAsyncInclude(id.Value);
            if (aInventory == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(aInventory);
        }

        // POST: AInventories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var aInventory = await this.aInventoryRepository.GetByIdAsync(id);
            try
            {
                await this.aInventoryRepository.DeleteAsync(aInventory);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }

            return RedirectToAction(nameof(Index));
        }

    }
}
