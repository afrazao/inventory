﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;
using Microsoft.AspNetCore.Authorization;

namespace Inventory.Web.Controllers
{
    public class CategoriesController : Controller
    {
        
        private readonly ICategoryRepository categoryRepository;

        public CategoriesController(ICategoryRepository categoryRepository)
        {
           
            this.categoryRepository = categoryRepository;
        }

        [Authorize(Roles ="Admin")]
        // GET: Categories
        public IActionResult Index()
        {
            return View(this.categoryRepository.GetAll());
        }

        [Authorize(Roles = "Admin")]
        // GET: Categories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var category = await this.categoryRepository.GetByIdAsync(id.Value);
            if (category == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(category);
        }

        [Authorize(Roles = "Admin")]
        // GET: Categories/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] Category category)
        {
            if (ModelState.IsValid)
            {
                await this.categoryRepository.CreateAsync(category);
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }

        [Authorize(Roles = "Admin")]
        // GET: Categories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var category = await this.categoryRepository.GetByIdAsync(id.Value);
            if (category == null)
            {
                return RedirectToAction("Error404", "Error");
            }
            return View(category);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] Category category)
        {
            if (id != category.Id)
            {
                return RedirectToAction("Error404", "Error");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await this.categoryRepository.UpdateAsync(category);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await this.categoryRepository.ExistsAsync(category.Id))
                    {
                        return RedirectToAction("Error404", "Error");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }

        [Authorize(Roles = "Admin")]
        // GET: Categories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var category = await this.categoryRepository.GetByIdAsync(id.Value);
            if (category == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(category);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var category = await this.categoryRepository.GetByIdAsync(id);
            try
            {
                await this.categoryRepository.DeleteAsync(category);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }
            return RedirectToAction(nameof(Index));
        }

       
    }
}
