﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Inventory.Web.Controllers.API
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class ClientsController : ControllerBase
    {
     
        private readonly IClientRepository clientRepository;

        public ClientsController(IClientRepository clientRepository)
        {
           
            this.clientRepository = clientRepository;
        }

        // GET: api/Clients
        [HttpGet]
        public IActionResult GetClients()
        {
            return Ok(this.clientRepository.GetAll());
        }

        // GET: api/Clients/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetClient([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var client = await this.clientRepository.GetByIdAsync(id);
            if (client == null)
            {
                return NotFound();
            }

            return Ok(client);
        }

        // PUT: api/Clients/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutClient([FromRoute] int id, [FromBody] Client client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != client.Id)
            {
                return BadRequest();
            }

          

            try
            {
                await this.clientRepository.UpdateAsync(client);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await this.clientRepository.ExistsAsync(client.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Clients
        [HttpPost]
        public async Task<IActionResult> PostClient([FromBody] Client client)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await this.clientRepository.CreateAsync(client);

            return CreatedAtAction("GetClient", new { id = client.Id }, client);
        }

        // DELETE: api/Clients/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteClient([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var client = await this.clientRepository.GetByIdAsync(id);
            if (client == null)
            {
                return NotFound();
            }

            try
            {
                await this.clientRepository.DeleteAsync(client);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }

            return Ok(client);
        }

    }
}