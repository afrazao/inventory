﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;

namespace Inventory.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class InventoryTypesController : ControllerBase
    {
       
        private readonly IAInventoryRepository aInventoryRepository;

        public InventoryTypesController(IAInventoryRepository aInventoryRepository)
        {
         
            this.aInventoryRepository = aInventoryRepository;
        }

        // GET: api/InventoryTypes
        [HttpGet]
        public IActionResult GetInventoryTypes()
        {
            return Ok(this.aInventoryRepository.GetInventoryTypes());
        }

        // GET: api/InventoryTypes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetInventoryType([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var inventoryType = await this.aInventoryRepository.GetInventoryTypeById(id);

            if (inventoryType == null)
            {
                return NotFound();
            }

            return Ok(inventoryType);
        }

        // PUT: api/InventoryTypes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInventoryType([FromRoute] int id, [FromBody] InventoryType inventoryType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != inventoryType.Id)
            {
                return BadRequest();
            }

          

            try
            {
                await this.aInventoryRepository.UpdateInventoryTypeAsync(inventoryType);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await this.aInventoryRepository.InventoryTypeExistsAsync(inventoryType.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/InventoryTypes
        [HttpPost]
        public async Task<IActionResult> PostInventoryType([FromBody] InventoryType inventoryType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await this.aInventoryRepository.CreateInventoryTypeAsync(inventoryType);

            return CreatedAtAction("GetInventoryType", new { id = inventoryType.Id }, inventoryType);
        }

        // DELETE: api/InventoryTypes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInventoryType([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var inventoryType = await this.aInventoryRepository.GetInventoryTypeById(id);
            if (inventoryType == null)
            {
                return NotFound();
            }

            try
            {
                await this.aInventoryRepository.DeleteInventoryTypeAsync(inventoryType);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }

            return Ok(inventoryType);
        }

     
    }
}