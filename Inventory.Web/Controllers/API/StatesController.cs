﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;

namespace Inventory.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatesController : ControllerBase
    {
      
       
        private readonly IAInventoryRepository aInventoryRepository;

        public StatesController(IAInventoryRepository aInventoryRepository)
        {
            
          
            this.aInventoryRepository = aInventoryRepository;
        }

        // GET: api/States
        [HttpGet]
        public IActionResult GetStates()
        {
            return Ok(this.aInventoryRepository.GetStates());
        }

        // GET: api/States/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetState([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var state = await this.aInventoryRepository.GetStateById(id);

            if (state == null)
            {
                return NotFound();
            }

            return Ok(state);
        }

        // PUT: api/States/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutState([FromRoute] int id, [FromBody] State state)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != state.Id)
            {
                return BadRequest();
            }

         

            try
            {
                await this.aInventoryRepository.UpdateStateAsync(state);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await this.aInventoryRepository.StateExistsAsync(state.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/States
        [HttpPost]
        public async Task<IActionResult> PostState([FromBody] State state)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await this.aInventoryRepository.CreateStateAsync(state);

            return CreatedAtAction("GetState", new { id = state.Id }, state);
        }

        // DELETE: api/States/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteState([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var state = await this.aInventoryRepository.GetStateById(id);
            if (state == null)
            {
                return NotFound();
            }

            try
            {
                await this.aInventoryRepository.DeleteStateAsync(state);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }

            return Ok(state);
        }

    
    }
}