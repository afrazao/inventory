﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;

namespace Inventory.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubscriptionTypesController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly ISubscriptionTypeRepository subscriptionTypeRepository;

        public SubscriptionTypesController(DataContext context,ISubscriptionTypeRepository subscriptionTypeRepository)
        {
            _context = context;
            this.subscriptionTypeRepository = subscriptionTypeRepository;
        }

        // GET: api/SubscriptionTypes
        [HttpGet]
        public IActionResult GetSubscriptionType()
        {
            return Ok(this.subscriptionTypeRepository.GetAll());
        }

        // GET: api/SubscriptionTypes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSubscriptionType([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subscriptionType = await this.subscriptionTypeRepository.GetByIdAsync(id);

            if (subscriptionType == null)
            {
                return NotFound();
            }

            return Ok(subscriptionType);
        }

        // PUT: api/SubscriptionTypes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubscriptionType([FromRoute] int id, [FromBody] SubscriptionType subscriptionType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != subscriptionType.Id)
            {
                return BadRequest();
            }

           

            try
            {
                await this.subscriptionTypeRepository.UpdateAsync(subscriptionType);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await this.subscriptionTypeRepository.ExistsAsync(subscriptionType.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SubscriptionTypes
        [HttpPost]
        public async Task<IActionResult> PostSubscriptionType([FromBody] SubscriptionType subscriptionType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await this.subscriptionTypeRepository.CreateAsync(subscriptionType);

            return CreatedAtAction("GetSubscriptionType", new { id = subscriptionType.Id }, subscriptionType);
        }

        // DELETE: api/SubscriptionTypes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSubscriptionType([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var subscriptionType = await this.subscriptionTypeRepository.GetByIdAsync(id);
            if (subscriptionType == null)
            {
                return NotFound();
            }

            try
            {
                await this.subscriptionTypeRepository.DeleteAsync(subscriptionType);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }

            return Ok(subscriptionType);
        }

      
    }
}