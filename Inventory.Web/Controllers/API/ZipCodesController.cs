﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;

namespace Inventory.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class ZipCodesController : ControllerBase
    {
        
        private readonly IZipCodeRepository zipCodeRepository;

        public ZipCodesController(IZipCodeRepository zipCodeRepository)
        {
            
            this.zipCodeRepository = zipCodeRepository;
        }

        // GET: api/ZipCodes
        [HttpGet]
        public IActionResult GetZipCode()
        {
            return Ok(this.zipCodeRepository.GetAll());
        }

        // GET: api/ZipCodes/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetZipCode([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var zipCode = await this.zipCodeRepository.GetByIdAsync(id);

            if (zipCode == null)
            {
                return NotFound();
            }

            return Ok(zipCode);
        }

        // PUT: api/ZipCodes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutZipCode([FromRoute] int id, [FromBody] ZipCode zipCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != zipCode.Id)
            {
                return BadRequest();
            }

            

            try
            {
                await this.zipCodeRepository.UpdateAsync(zipCode);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await this.zipCodeRepository.ExistsAsync(zipCode.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ZipCodes
        [HttpPost]
        public async Task<IActionResult> PostZipCode([FromBody] ZipCode zipCode)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await this.zipCodeRepository.CreateAsync(zipCode);

            return CreatedAtAction("GetZipCode", new { id = zipCode.Id }, zipCode);
        }

        // DELETE: api/ZipCodes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteZipCode([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var zipCode = await this.zipCodeRepository.GetByIdAsync(id);
            if (zipCode == null)
            {
                return NotFound();
            }

            try
            {
                await this.zipCodeRepository.DeleteAsync(zipCode);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }

            return Ok(zipCode);
        }

      
    }
}