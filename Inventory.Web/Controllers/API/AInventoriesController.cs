﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;
using Inventory.Web.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Inventory.Web.Controllers.API
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]//Para haver autenticação utilizamos um token do tipo bearer, neste caso.
    [ApiController]
    public class AInventoriesController : ControllerBase
    {
       
        private readonly IAInventoryRepository aInventoryRepository;
        private readonly IUserHelper userHelper;
        private readonly IClientRepository clientRepository;
     
        

        public AInventoriesController( IAInventoryRepository aInventoryRepository, IUserHelper userHelper,IClientRepository clientRepository)
        {
           
            this.aInventoryRepository = aInventoryRepository;
            this.userHelper = userHelper;
            this.clientRepository = clientRepository;
          
        }

        // GET: api/AInventories
        [HttpGet("{username}")]
        public IActionResult GetInventories([FromRoute] string username)
        {

         

            if (username == "jarinventory@gmail.com")
            {
                return Ok(this.aInventoryRepository.GetAllWithClients());
            }
            else
            {
                return Ok(this.aInventoryRepository.GetThisClientInventories(username));
            }

        }

        //// GET: api/AInventories/5
        //[HttpGet("{id}")]
        //public async Task<IActionResult> GetAInventory([FromRoute] int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var aInventory = await this.aInventoryRepository.GetByIdAsync(id
        //        );

        //    if (aInventory == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(aInventory);
        //}

        // PUT: api/AInventories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAInventory([FromRoute] int id, [FromBody] AInventory aInventory)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState);
            }

            if (id != aInventory.Id)
            {
                return BadRequest();
            }

            var oldInventory = await this.aInventoryRepository.GetByIdAsync(id);
            if (oldInventory == null)
            {
                return this.BadRequest("Inventory doesn´t exist");
            }

            //TODO: Upload Images
            oldInventory.Name = aInventory.Name;
            oldInventory.Date = aInventory.Date;
            oldInventory.Client = aInventory.Client;
            oldInventory.InventoryType = aInventory.InventoryType;
            oldInventory.State = aInventory.State;

            var updatedInventory = await this.aInventoryRepository.UpdateAsync(oldInventory);

            return Ok(updatedInventory);
        }

        // POST: api/AInventories
        [HttpPost]
        public async Task<IActionResult> PostAInventory([FromBody] Common.Models.Invent aInventory)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(ModelState);
            }

            var user = await this.userHelper.GetUserByEmailAsync(aInventory.Client.User.UserName);
            var client =  this.clientRepository.GetClientByUserName(aInventory.Client.User.UserName);
            
            var state = await this.aInventoryRepository.GetStateById(client.StateId);
            var inventoryType = await this.aInventoryRepository.GetInventoryTypeById(aInventory.InventoryType.Id);
            if (user == null)
            {
                return this.BadRequest("Invalid user");
            }


            //TODO:upload images
            var entityInventory = new AInventory
            {
                Date = aInventory.Date,
                State = state,
                InventoryType = inventoryType,
                Name = aInventory.Name,
                Client = client,

            };


            var newInventorie = await this.aInventoryRepository.CreateAsync(entityInventory);
            return Ok(newInventorie);

           
        }

        // DELETE: api/AInventories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAInventory([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var aInventory = await this.aInventoryRepository.GetByIdAsync(id);
            if (aInventory == null)
            {
                return this.NotFound();
            }

            try
            {
                await this.aInventoryRepository.DeleteAsync(aInventory);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }
           

            return Ok(aInventory);
        }

      
    }
}