﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;

namespace Inventory.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class InventoryArticlesController : ControllerBase
    {
      
        private readonly IAInventoryRepository inventoryArticleRepository;

        public InventoryArticlesController(IAInventoryRepository inventoryArticleRepository)
        {
            
            this.inventoryArticleRepository = inventoryArticleRepository;
        }

        // GET: api/InventoryArticles
        [HttpGet("{inventoryId}")]
        public IActionResult GetInventoryArticles([FromRoute] int inventoryId)
        {
            if(inventoryId == 0)
            {
                return NotFound();
            }
            return Ok(this.inventoryArticleRepository.GetThisInventoryInventoryArticles(inventoryId));
        }

        //// GET: api/InventoryArticles/5
        //[HttpGet("{id}")]
        //public async Task<IActionResult> GetInventoryArticle([FromRoute] int id)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    var inventoryArticle = await this.inventoryArticleRepository.GetInventoryArticleById(id);

        //    if (inventoryArticle == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(inventoryArticle);
        //}

        // PUT: api/InventoryArticles/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInventoryArticle([FromRoute] int id, [FromBody] InventoryArticle inventoryArticle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != inventoryArticle.Id)
            {
                return BadRequest();
            }

            

            try
            {
                await this.inventoryArticleRepository.UpdateInventoryArticleAsync(inventoryArticle);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await this.inventoryArticleRepository.InventoryArticleExistsAsync(inventoryArticle.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/InventoryArticles
        [HttpPost]
        public async Task<IActionResult> PostInventoryArticle([FromBody] InventoryArticle inventoryArticle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await this.inventoryArticleRepository.CreateInventoryArticleAsync(inventoryArticle);

            return CreatedAtAction("GetInventoryArticle", new { id = inventoryArticle.Id }, inventoryArticle);
        }

        // DELETE: api/InventoryArticles/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInventoryArticle([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var inventoryArticle =  await this.inventoryArticleRepository.GetInventoryArticleById(id);
            if (inventoryArticle == null)
            {
                return NotFound();
            }

            try
            {
                await this.inventoryArticleRepository.DeleteInventoryArticleAsync(inventoryArticle);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }

            return Ok(inventoryArticle);
        }

       
    }
}