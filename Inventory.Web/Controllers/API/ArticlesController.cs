﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;

namespace Inventory.Web.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticlesController : ControllerBase
    {
     
        private readonly IArticleRepository articleRepository;

        public ArticlesController(IArticleRepository articleRepository)
        {
           
            this.articleRepository = articleRepository;
        }

        // GET: api/Articles
        [HttpGet]
        public IActionResult GetArticles()
        {
            return Ok(this.articleRepository.GetAll());
        }

        // GET: api/Articles/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetArticle([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var article = await this.articleRepository.GetByIdAsync(id);

            if (article == null)
            {
                return NotFound();
            }

            return Ok(article);
        }

        // PUT: api/Articles/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutArticle([FromRoute] int id, [FromBody] Article article)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != article.Id)
            {
                return BadRequest();
            }

           

            try
            {
                await this.articleRepository.UpdateAsync(article);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!await this.articleRepository.ExistsAsync(article.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Articles
        [HttpPost]
        public async Task<IActionResult> PostArticle([FromBody] Article article)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await this.articleRepository.CreateAsync(article);

            return CreatedAtAction("GetArticle", new { id = article.Id }, article);
        }

        // DELETE: api/Articles/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteArticle([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var article = await this.articleRepository.GetByIdAsync(id);
            if (article == null)
            {
                return NotFound();
            }

            try
            {
                await this.articleRepository.DeleteAsync(article);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }

            return Ok(article);
        }

     
    }
}