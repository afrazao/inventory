﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Models;
using Inventory.Web.Helpers;
using Inventory.Web.Data.IRepositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;

namespace Inventory.Web.Controllers
{
    public class ViewersController : Controller
    {
       
        private readonly IViewerRepository viewerRepository;
        private readonly IClientRepository clientRepository;
        private readonly IUserHelper userHelper;
        public string roleName;
        private readonly EmailSender emailSender;

        public ViewersController(IViewerRepository viewerRepository,IClientRepository clientRepository,IUserHelper userHelper)
        {
            
            this.viewerRepository = viewerRepository;
            this.clientRepository = clientRepository;
            this.userHelper = userHelper;
            this.emailSender = new EmailSender();
        }

        [Authorize(Roles ="InventoryAdmin")]
        //CLIENT VÊ TODOS OS VIEWERS QUE CRIOU
        // GET: Viewers
        public IActionResult Index(/*int id*/)
        {
            var userIdentity = this.userHelper.GetUserByEmail(User.Identity.Name);
            var client = this.clientRepository.GetClientByUserName(User.Identity.Name);
            var viewer = this.viewerRepository.GetViewerByUserName(User.Identity.Name);
            if (client == null && viewer == null && User.Identity.Name != "jarinventory@gmail.com")
            {
                return this.RedirectToAction("Create", "Clients");
            }

            var user = this.clientRepository.GetClientByUserName(User.Identity.Name);

            var ViewersListByClient = this.viewerRepository.GetIdViewersListClient(user.Id);


            var ClientViewersToList = this.viewerRepository.GetViewersList(ViewersListByClient);

            List<object> listdata = new List<object>(); 

            foreach (var item in ClientViewersToList)
            {
                listdata.Add(new
                {
                    text = item.Email,
                    id = item.Id,
                    imgUrl = "",
                    icon = this.clientRepository.GetClientFirstLetter(item.Email),
                    //contact = item.Contact,
                    //category = "Information",
                    child = new List<object>() { new { text = "Details", id = item.Id, imgUrl = "", icon = "D", category = "Information", child = "" },
                    new { text = "Change Permission", id = item.Id, imgUrl = "", icon = "P", category = "Information", child = "" },
                    new { text = "Inventories", id = item.Id, imgUrl = "", icon = "I", category = "Information", child = "" }
                    }
                });
            }

            ViewBag.listData = listdata;

            //ViewBag.IdInventory = id;

            return View();
        }

        [Authorize(Roles = "InventoryAdmin")]
        // GET: Viewers
        public IActionResult IndexAdmin(int id)
        {

            var ViewersByInventoryList = this.viewerRepository.GetIdViewersListNotInInventory(id, User.Identity.Name);

            if(ViewersByInventoryList.Count() == 0)
            {
                return RedirectToAction("Create", new { id = id });
            }

            var ViewersToList = this.viewerRepository.GetViewersList(ViewersByInventoryList);

            List<object> listdata = new List<object>();

            foreach (var item in ViewersToList)
            {
                listdata.Add(new
                {
                    text = item.Email,
                    id = item.Id,
                    imgUrl = "",
                    icon = this.clientRepository.GetClientFirstLetter(item.Email),
                    //contact = item.Contact,
                    //category = "Information",
                    child = new List<object>() { new { text = "Add" , id = item.Id, imgUrl = "", icon = this.clientRepository.GetClientFirstLetter(item.Email), category = "Information", child = "" },
                    //new { text = item.StateId, id = item.Id, imgUrl = "", icon = "N", category = "Information", child = "" },
                    //new { text = "Inventories", id = item.Id, imgUrl = "", icon = "N", category = "Information", child = "" }
                    }
                });
            }

            ViewBag.listData = listdata;

            ViewBag.IdInventory = id;

            return View();
        }

        [Authorize(Roles = "InventoryAdmin, Admin")]
        // GET: Viewers/Details/5
        public async Task<IActionResult> Details(string username)
        {
            var id = username;

            var getViewer = this.viewerRepository.GetViewerByUserName(username);

            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var viewer = await this.viewerRepository.GetByIdAsyncInclude(getViewer.Id);
            if (viewer == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(viewer);
        }

        [Authorize(Roles = "InventoryAdmin")]
        // GET: Viewers/Create
        public IActionResult Create(int id)
        {
            ViewBag.IdInventory = id;

            var PermissionsList = PermissionsHelper.PermissionsList();
            ViewData["Permissions"] = new SelectList(PermissionsList);
            //ViewData["ClientId"] = new SelectList(this.clientRepository.GetAll(), "Id", "Name");
            return View();
        }

        // POST: Viewers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,LastName,Permission,Email,Contact")] Viewer viewer, int id)
        {
            if (ModelState.IsValid == false)
            {
                var thisPermissionsList = PermissionsHelper.PermissionsList();
                ViewData["Permissions"] = new SelectList(thisPermissionsList);
                //ViewData["ClientId"] = new SelectList(this.clientRepository.GetAll(), "Id", "Name", viewer.ClientId);
                return View(viewer);
                //ModelState.AddModelError(string.Empty, "You have to fill all the fields correctly");
            }

            var viewerEmail = this.viewerRepository.GetViewerByUserName(viewer.Email);

            if (viewerEmail != null)
            {
                ModelState.AddModelError(string.Empty, "The viewer already exists in our database. For now it is not possible to create the viewer.");
                var thisPermissionsList = PermissionsHelper.PermissionsList();
                ViewData["Permissions"] = new SelectList(thisPermissionsList);
                //ViewData["ClientId"] = new SelectList(this.clientRepository.GetAll(), "Id", "Name", viewer.ClientId);
                return View(viewer);
            }

            var checkEmail = await this.userHelper.GetUserByEmailAsync(viewer.Email);

            if (checkEmail != null)
            {
                if (checkEmail.Email == User.Identity.Name)
                {
                    ModelState.AddModelError("Email", "Your viewer´s email must be different than yours");
                    var thisPermissionsList = PermissionsHelper.PermissionsList();
                    ViewData["Permissions"] = new SelectList(thisPermissionsList);
                    //ViewData["ClientId"] = new SelectList(this.clientRepository.GetAll(), "Id", "Name", viewer.ClientId);
                    return View(viewer);
                }
                else
                {
                    viewer.User = await this.userHelper.GetUserByEmailAsync(viewer.Email);
                    var clientId = this.clientRepository.GetClientByUserName(User.Identity.Name);
                    viewer.ClientId = clientId.Id;
                    await this.userHelper.AddUserToRoleAsync(viewer.User, viewer.Permission.ToString());
                    await this.viewerRepository.CreateAsync(viewer);
                    var ViewerForId = this.viewerRepository.GetViewerByUserName(viewer.Email);
                    var Vid = ViewerForId.Id;
                    var viewerInventory = new ViewerInventories
                    {
                        AInventoryId = id,
                        ViewerId = Vid,
                        Name = viewer.Name
                    };
                    await this.viewerRepository.CreateViewerInventoryAsync(viewerInventory);

                    var client = this.clientRepository.GetClientByUserName(User.Identity.Name);

                    await emailSender.SendEmailAsync(
                        viewer.Email,
                        "Your Inventory Now Viewer Account",
                        $"A Viewer account has been opened for you by the Client {client.Name} {client.LastName}.");

                    return RedirectToAction("Index", "ViewerInventories", new { id = id });
                }

            }

            if (ModelState.IsValid)
            {
                var client = this.clientRepository.GetClientByUserName(User.Identity.Name);
                viewer.ClientId = client.Id;
                var user = new User
                {
                    FirstName = viewer.Name,
                    LastName = viewer.LastName,
                    Email = viewer.Email,
                    UserName = viewer.Email,
                    EmailConfirmed = true,
                };
                var result = await this.userHelper.AddUserAsync(user, "Senha123!");
                if (result != IdentityResult.Success)
                {
                    ModelState.AddModelError("Email", "The email already exists");
                    var thisPermissionsList = PermissionsHelper.PermissionsList();
                    ViewData["Permissions"] = new SelectList(thisPermissionsList);
                    //ViewData["ClientId"] = new SelectList(this.clientRepository.GetAll(), "Id", "Name", viewer.ClientId);
                    return View(viewer);
                }
                viewer.User = await this.userHelper.GetUserByEmailAsync(viewer.Email);
                await this.userHelper.AddUserToRoleAsync(user, viewer.Permission.ToString());
                await this.viewerRepository.CreateAsync(viewer);
                var ViewerForId = this.viewerRepository.GetViewerByUserName(viewer.Email);
                var Vid = ViewerForId.Id;
                var viewerInventory = new ViewerInventories
                {
                    AInventoryId = id,
                    ViewerId = Vid,
                    Name = viewer.Name
                };
                await this.viewerRepository.CreateViewerInventoryAsync(viewerInventory);

                var userToMail = await this.userHelper.GetUserByEmailAsync(viewer.Email);
                string ctoken = userHelper.GetGeneratePasswordResetTokenAsync(user).Result;
                string ctokenlink = Url.Action("ResetPassword", "Account", new
                {
                    userid = userToMail.Id,
                    token = ctoken
                }, protocol: HttpContext.Request.Scheme);

                await emailSender.SendEmailAsync(
                    user.Email,
                    "Your Inventory Now Viewer Account - Password Creation",
                    $"An account has been opened for you by the Client {client.Name} {client.LastName}. Please click on the link to create your password: {ctokenlink}");

                return RedirectToAction("Index", "ViewerInventories", new { id = id });
            }
            var PermissionsList = PermissionsHelper.PermissionsList();
            ViewData["Permissions"] = new SelectList(PermissionsList);
            //ViewData["ClientId"] = new SelectList(this.clientRepository.GetAll(), "Id", "Name", viewer.ClientId);
            return View(viewer);
        }

        [Authorize(Roles = "InventoryAdmin")]
        public async Task<IActionResult> AddViewerToInventory(int id, string username)
        {
            var ViewerForId = this.viewerRepository.GetViewerByUserName(username);
            var viewerInventory = new ViewerInventories
            {
                AInventoryId = id,
                ViewerId = ViewerForId.Id,
                Name = ViewerForId.Name
            };
            await this.viewerRepository.CreateViewerInventoryAsync(viewerInventory);
            return RedirectToAction("Index", "ViewerInventories", new { id = id });
        }

        [Authorize(Roles = "InventoryAdmin")]
        public async Task<IActionResult> RemoveViewerFromInventory(int id, string username)
        {
            var ViewerForId = this.viewerRepository.GetViewerByUserName(username);

            var viewerInventoryToRemove = await this.viewerRepository.GetViewerInventoryByInventoryAndViewer(id, ViewerForId.Id);

            await this.viewerRepository.DeleteViewerInventoriesAsync(viewerInventoryToRemove);
            return RedirectToAction("Index", "ViewerInventories", new { id = id });
        }

        [Authorize(Roles = "InventoryAdmin")]
        // GET: Viewers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var viewer = await this.viewerRepository.GetByIdAsync(id.Value);
            if (viewer == null)
            {
                return RedirectToAction("Error404", "Error");
            }
            var PermissionsList = PermissionsHelper.PermissionsList();
            ViewData["Permissions"] = new SelectList(PermissionsList);
            ViewData["ClientId"] = new SelectList(this.clientRepository.GetAll(), "Id", "Name", viewer.ClientId);
           
            return View(viewer);
        }

        // POST: Viewers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,LastName,ClientId,Permission,Email,Contact")] Viewer viewer)
        {
            if (id != viewer.Id)
            {
                return RedirectToAction("Error404", "Error");
            }
            var checkEmail = await this.userHelper.GetUserByEmailAsync(viewer.Email);

            if (checkEmail.Email == User.Identity.Name)
            {
                ModelState.AddModelError("Email", "Your viewer´s email must be different than yours");
                var thisPermissionsList = PermissionsHelper.PermissionsList();
                ViewData["Permissions"] = new SelectList(thisPermissionsList);
                //ViewData["ClientId"] = new SelectList(this.clientRepository.GetAll(), "Id", "Name", viewer.ClientId);
                return View(viewer);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    viewer.Client = this.clientRepository.GetClientByUserName(User.Identity.Name);

                    viewer.ClientId = viewer.Client.Id;
                    viewer.User = await this.userHelper.GetUserByEmailAsync(User.Identity.Name);
                    viewer.UserId = viewer.User.Id;
                    await this.viewerRepository.UpdateAsync(viewer);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await this.viewerRepository.ExistsAsync(viewer.Id))
                    {
                        return RedirectToAction("Error404", "Error");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            var PermissionsList = PermissionsHelper.PermissionsList();
            ViewData["Permissions"] = new SelectList(PermissionsList);
            ViewData["ClientId"] = new SelectList(this.clientRepository.GetAll(), "Id", "Name", viewer.ClientId);
            return View(viewer);
        }

        [NonAction]
        // GET: Viewers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var viewer = await this.viewerRepository.GetByIdAsyncInclude(id.Value);
            if (viewer == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(viewer);
        }

        // POST: Viewers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var viewer = await this.viewerRepository.GetByIdAsyncInclude(id);
            try
            {
                await this.viewerRepository.DeleteAsync(viewer);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "InventoryAdmin")]
        public IActionResult EditRole(string username)
        {
            ViewBag.username = username;

            var PermissionsList = PermissionsHelper.PermissionsList();
            ViewData["Permissions"] = new SelectList(PermissionsList);

            var roleList = this.userHelper.GetViewerRoles(username);

            foreach (var item in roleList)
            {
                var roleN = this.userHelper.GetRoleName(item.RoleId);

                if (roleN == "InventoryView" || roleN == "InventoryCRU")
                {
                    roleName = roleN;
                }
            }

            if(roleName == "InventoryView")
            {
                ViewBag.role = roleName;
            }
            else
            {
                ViewBag.role = roleName;
            }

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> EditRole(EditViewerRoleViewModel view)
        {
            var user = view.UserName;

            var roleList = this.userHelper.GetViewerRoles(user);

            foreach (var item in roleList)
            {
                var roleN = this.userHelper.GetRoleName(item.RoleId);

                if(roleN == "InventoryView" || roleN == "InventoryCRU")
                {
                    roleName = roleN;
                }
            }

            var role = view.Role;

            if(role != roleName)
            {
                var userToRemoveAndAdd = this.userHelper.GetUserByEmail(view.UserName);
                await this.userHelper.RemoveUserFromRoleAsync(userToRemoveAndAdd, roleName);

                await this.userHelper.AddUserToRoleAsync(userToRemoveAndAdd, role);
            }

            return RedirectToAction("Index");
        }
    }
}
