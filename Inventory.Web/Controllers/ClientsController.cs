﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;
using Inventory.Web.Data.Repositories;
using Inventory.Web.Helpers;
using Inventory.Web.Models;
using System.IO;
using Newtonsoft.Json;
using Syncfusion.EJ2.Navigations;
using Microsoft.AspNetCore.Authorization;

namespace Inventory.Web.Controllers
{
    public class ClientsController : Controller
    {

        private readonly IClientRepository clientRepository;
        private readonly IAInventoryRepository aInventoryRepository;
        private readonly ISubscriptionTypeRepository subscriptionTypeRepository;
        private readonly IUserHelper userHelper;
        private readonly IZipCodeRepository zipCodeRepository;
        private readonly IViewerRepository viewerRepository;

        public ClientsController(IClientRepository clientRepository, IAInventoryRepository aInventoryRepository,
            ISubscriptionTypeRepository subscriptionTypeRepository, IUserHelper userHelper, IZipCodeRepository zipCodeRepository, IViewerRepository viewerRepository)
        {

            this.clientRepository = clientRepository;
            this.aInventoryRepository = aInventoryRepository;
            this.subscriptionTypeRepository = subscriptionTypeRepository;
            this.userHelper = userHelper;
            this.zipCodeRepository = zipCodeRepository;
            this.viewerRepository = viewerRepository;
        }

        [NonAction]
        // GET: Clients
        public IActionResult Index()
        {

            return View(this.clientRepository.GetClientsInclude());
        }

        [Authorize(Roles = "Admin")]
        // GET: Clients
        public IActionResult IndexAdmin()
        {
            var ClientsList = this.clientRepository.GetClientsList();

            List<object> listdata = new List<object>();

            foreach (var item in ClientsList)
            {
                listdata.Add(new
                {
                    text = item.Email,
                    id = item.Id,
                    imgUrl = "",
                    icon = this.clientRepository.GetClientFirstLetter(item.Email),
                    //contact = item.Contact,
                    //category = "Information",
                    child = new List<object>() { new { text = "Details" , id = item.Id, imgUrl = "", icon = "D", category = "Information", child = "" },
                    new { text = "Change State", id = item.Id, imgUrl = "", icon = "S", category = "Information", child = "" },
                    new { text = "Inventories", id = item.Id, imgUrl = "", icon = "I", category = "Information", child = "" }
                    }
                });
            }

            ViewBag.listData = listdata;

            return View();
        }

        [Authorize(Roles = "Admin, InventoryAdmin")]
        // GET: Clients/Details/5
        public async Task<IActionResult> Details(string username)
        {
            var user = this.userHelper.GetUserByEmail(User.Identity.Name);
            var clientCheck = this.clientRepository.GetClientByUserName(User.Identity.Name);
            var viewer = this.viewerRepository.GetViewerByUserName(User.Identity.Name);
            if (clientCheck == null && viewer == null && User.Identity.Name != "jarinventory@gmail.com")
            {
                return this.RedirectToAction("Create", "Clients");
            }

            var id = this.clientRepository.GetClientByUserName(username);

            if (id == null)
            {
                id = this.clientRepository.GetClientByUserName(User.Identity.Name);
            }

            var client = await this.clientRepository.GetByIdAsyncInclude(id.Id);
            return View(client);
        }

        [Authorize(Roles = "InventoryAdmin")]
        // GET: Clients/Create
        public IActionResult Create()
        {
            User email = this.userHelper.GetUserByEmail(User.Identity.Name);
            var findClient = this.clientRepository.GetClientByUserName(User.Identity.Name);           
            if(findClient!=null)
            {
                var clientId = findClient.Id;
                return RedirectToAction("Details", "Clients", clientId);
            }
            var name = email.FirstName;
            var lastname = email.LastName;
            ViewBag.Name = name;
            ViewBag.LastName = lastname;
            ViewData["SubscriptionTypeId"] = new SelectList(this.subscriptionTypeRepository.GetAll(), "Id", "Name");
            //ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City");
            return View();
        }

        // POST: Clients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,LastName,ZipCodeId,Address,TIN,Email,Contact,SubscriptionTypeId,ImageFile")] ClientViewModel view)
        {

            var findClient = this.clientRepository.GetClientByUserName(User.Identity.Name);

            if(findClient != null)
            {
                ModelState.AddModelError("Email", "This Client already exists");
                ViewData["StateId"] = new SelectList(this.aInventoryRepository.GetStates(), "Id", "Name", view.StateId);
                ViewData["SubscriptionTypeId"] = new SelectList(this.subscriptionTypeRepository.GetAll(), "Id", "Name", view.SubscriptionTypeId);
                ViewData["UserId"] = new SelectList(this.userHelper.GetAllUsers(), "Id", "Id", view.UserId);
                ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City", view.ZipCodeId);
                return View(view);
            }


            string tin = Request.Form["TIN"];

            if (string.IsNullOrEmpty(tin) == true || tin.Length != 9 || RegexHelper.IsValidContrib(tin) == false)
            {
                User email = this.userHelper.GetUserByEmail(User.Identity.Name);
                var name = email.FirstName;
                var lastname = email.LastName;
                ViewBag.Name = name;
                ViewBag.LastName = lastname;
                ViewData["SubscriptionTypeId"] = new SelectList(this.subscriptionTypeRepository.GetAll(), "Id", "Name");
                ModelState.AddModelError(string.Empty, "The TIN number is not valid");
                return View(view);
            }

            string addressCheck = Request.Form["Address"];

            if (string.IsNullOrEmpty(addressCheck) || addressCheck == "Please insert your street name")
            {
                User email = this.userHelper.GetUserByEmail(User.Identity.Name);
                var name = email.FirstName;
                var lastname = email.LastName;
                ViewBag.Name = name;
                ViewBag.LastName = lastname;
                ViewData["SubscriptionTypeId"] = new SelectList(this.subscriptionTypeRepository.GetAll(), "Id", "Name");
                ModelState.AddModelError("Addressy", "You have to insert an address.");
                return View();
            }

            string code = Request.Form["ZipCode"];
       


            if(code == "0000-000" || string.IsNullOrEmpty(code))
            {
                User email = this.userHelper.GetUserByEmail(User.Identity.Name);
                var name = email.FirstName;
                var lastname = email.LastName;
                ViewBag.Name = name;
                ViewBag.LastName = lastname;
                ViewData["SubscriptionTypeId"] = new SelectList(this.subscriptionTypeRepository.GetAll(), "Id", "Name");
                ModelState.AddModelError("ZipCode", "You have to insert a valid ZipCode.");
                return View();
            }

            string sub = code.Substring(4,1);

            if (sub != "-")
            {
                User email = this.userHelper.GetUserByEmail(User.Identity.Name);
                var name = email.FirstName;
                var lastname = email.LastName;
                ViewBag.Name = name;
                ViewBag.LastName = lastname;
                ViewData["SubscriptionTypeId"] = new SelectList(this.subscriptionTypeRepository.GetAll(), "Id", "Name");
                ModelState.AddModelError("ZipCode", "You have to insert a valid ZipCode.");
                return View();
            }

            var getZipCode = this.zipCodeRepository.GetZipCodeByAddress(addressCheck);

            if(getZipCode!=null)
            {
                view.ZipCode = getZipCode;
            }

            var zipCode = new ZipCode
            {
                Code = code,
                City = "Lisboa",
                Country = "Portugal",
                Name = addressCheck,
                longitude = "0",
                latitude = "0"
            };


            await this.zipCodeRepository.CreateAsync(zipCode);
            view.ZipCode = zipCode;
        

            if (ModelState.IsValid)
            {
                var path = string.Empty;
                if (view.ImageFile != null && view.ImageFile.Length > 0)
                {
                    var guid = Guid.NewGuid().ToString();
                    var file = $"{guid}.jpg";
                    path = Path.Combine(
                        Directory.GetCurrentDirectory(),
                        "wwwroot\\images\\Clients",
                        file);


                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await view.ImageFile.CopyToAsync(stream);
                    }


                    path = $"~/images/Clients/{file}";
                }

              
                var user = await this.userHelper.GetUserByEmailAsync(User.Identity.Name);
                    view.UserId = user.Id;
                    var state = this.aInventoryRepository.GetStateByName("Opened");
                    if (state != null)
                    {
                        view.StateId = state.Id;
                    }

                    var client = this.ToClient(view, path);
                    //TODO: validar state
                    await this.clientRepository.CreateAsync(client);
                //await this.userHelper.AddUserToRoleAsync(user, "InventoryAdmin");
                return RedirectToAction("Index", "Home");
               
            }
                ViewData["StateId"] = new SelectList(this.aInventoryRepository.GetStates(), "Id", "Name", view.StateId);
                ViewData["SubscriptionTypeId"] = new SelectList(this.subscriptionTypeRepository.GetAll(), "Id", "Name", view.SubscriptionTypeId);
                ViewData["UserId"] = new SelectList(this.userHelper.GetAllUsers(), "Id", "Id", view.UserId);
                ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City", view.ZipCodeId);
                return View(view);
           
        }

       

        private Client ToClient(ClientViewModel view, string path)
        {
            return new Client
            {
                Id = view.Id,
                ImageUrl = path,
                LastName=view.LastName,
                UserId=view.UserId,
                User=view.User,
                Address=view.Address,
                ZipCode=view.ZipCode,
                ZipCodeId=view.ZipCodeId,
                TIN=view.TIN,
                State=view.State,
                StateId=view.StateId,
                Email=view.Email,
                Contact=view.Contact,
                SubscriptionType=view.SubscriptionType,
                SubscriptionTypeId=view.SubscriptionTypeId,
                Name = view.Name
            };

            //ViewData["StateId"] = new SelectList(this.aInventoryRepository.GetStates(), "Id", "Name", client.StateId);
            //ViewData["SubscriptionTypeId"] = new SelectList(this.subscriptionTypeRepository.GetAll(), "Id", "Name", client.SubscriptionTypeId);
            //ViewData["UserId"] = new SelectList(this.userHelper.GetAllUsers(), "Id", "Id", client.UserId);
            //ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City", client.ZipCodeId);
            //return View(client);
        }

        [Authorize(Roles = "InventoryAdmin")]
        // GET: Clients/Edit/5
        public async Task<IActionResult> Edit(int? id)
            {
                if (id == null)
                {
                return RedirectToAction("MyBadRequest", "Error");
                }

                var client = await this.clientRepository.GetByIdAsync(id.Value);
                if (client == null)
                {
                return RedirectToAction("Error404", "Error");
            }
            var view = this.ToClientViewModel(client);
            ViewData["StateId"] = new SelectList(this.aInventoryRepository.GetStates(), "Id", "Name", view.StateId);
                ViewData["SubscriptionTypeId"] = new SelectList(this.subscriptionTypeRepository.GetAll(), "Id", "Name", view.SubscriptionTypeId);
                ViewData["UserId"] = new SelectList(this.userHelper.GetAllUsers(), "Id", "Id", view.UserId);
                ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "Code", view.ZipCodeId);
                return View(view);
            }

        private ClientViewModel ToClientViewModel(Client client)
        {
            return new ClientViewModel
            {
                
                      Id = client.Id,
                      ImageUrl = client.ImageUrl,
                      LastName = client.LastName,
                      UserId = client.UserId,
                      User = client.User,
                      Address = client.Address,
                      ZipCode = client.ZipCode,
                      ZipCodeId = client.ZipCodeId,
                      TIN = client.TIN,
                      State = client.State,
                      StateId = client.StateId,
                      Email = client.Email,
                      Contact = client.Contact,
                      SubscriptionType = client.SubscriptionType,
                      SubscriptionTypeId = client.SubscriptionTypeId,
                      Name = client.Name
             };
       
        }



        // POST: Clients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,LastName,UserId,Address,ZipCodeId,TIN,StateId,Email,Contact,SubscriptionTypeId,ImageFile")] ClientViewModel view)
        {
            if (id != view.Id)
            {
                return RedirectToAction("Error404", "Error");
            }

            string tin = Request.Form["TIN"];



            string addressCheck = Request.Form["Address"];

            if (string.IsNullOrEmpty(addressCheck) || addressCheck == "Please insert your street name")
            {
                User email = this.userHelper.GetUserByEmail(User.Identity.Name);
                var name = email.FirstName;
                var lastname = email.LastName;
                ViewBag.Name = name;
                ViewBag.LastName = lastname;
                ViewData["SubscriptionTypeId"] = new SelectList(this.subscriptionTypeRepository.GetAll(), "Id", "Name");
                ModelState.AddModelError("Address", "You have to insert an address.");
                return View();
            }



            var getZip = this.zipCodeRepository.GetZipCodeByAddress(addressCheck);
            
            if(getZip!=null)
            {
                view.ZipCode = getZip;
            }



           



            if (string.IsNullOrEmpty(tin) == true || tin.Length != 9 || RegexHelper.IsValidContrib(tin) == false)
            {
                User email = this.userHelper.GetUserByEmail(User.Identity.Name);
                var name = email.FirstName;
                var lastname = email.LastName;
                ViewBag.Name = name;
                ViewBag.LastName = lastname;
                var client = await this.clientRepository.GetByIdAsync(id);
                if (client == null)
                {
                    return RedirectToAction("Error404", "Error");
                }
                var viewClient = this.ToClientViewModel(client);
                ViewData["StateId"] = new SelectList(this.aInventoryRepository.GetStates(), "Id", "Name", viewClient.StateId);
                ViewData["SubscriptionTypeId"] = new SelectList(this.subscriptionTypeRepository.GetAll(), "Id", "Name", viewClient.SubscriptionTypeId);
                ViewData["UserId"] = new SelectList(this.userHelper.GetAllUsers(), "Id", "Id", viewClient.UserId);
                ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City", viewClient.ZipCodeId);
                ModelState.AddModelError(string.Empty, "The TIN number is not valid");
                return View(view);
            }

            if (ModelState.IsValid)
            {
                try
                {

                    var path = string.Empty;
                    if (view.ImageFile != null && view.ImageFile.Length > 0)
                    {
                        var guid = Guid.NewGuid().ToString();
                        var file = $"{guid}.jpg";
                        path = Path.Combine(
                            Directory.GetCurrentDirectory(),
                            "wwwroot\\images\\Clients",
                            file);


                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await view.ImageFile.CopyToAsync(stream);
                        }


                        path = $"~/images/Clients/{file}";
                    }

                        var thisZipAddress = Request.Form["Address"];
                        var zipCodeId = this.zipCodeRepository.GetZipCodeByAddress(thisZipAddress);
                        view.ZipCodeId = zipCodeId.Id;
                        var user = await this.userHelper.GetUserByEmailAsync(User.Identity.Name);
                        view.UserId = user.Id;
                        var state = this.aInventoryRepository.GetStateByName("Opened");
                        if (state != null)
                        {
                            view.StateId = state.Id;
                        }

                        var client = this.ToClient(view, path);
                      
                 
                    await this.clientRepository.UpdateAsync(client);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await this.clientRepository.ExistsAsync(view.Id))
                    {
                        return RedirectToAction("Error404", "Error");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details","Clients", view.Id);
            }
            ViewData["StateId"] = new SelectList(this.aInventoryRepository.GetStates(), "Id", "Name", view.StateId);
            ViewData["SubscriptionTypeId"] = new SelectList(this.subscriptionTypeRepository.GetAll(), "Id", "Name", view.SubscriptionTypeId);
            ViewData["UserId"] = new SelectList(this.userHelper.GetAllUsers(), "Id", "Id", view.UserId);
            ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City", view.ZipCodeId);
            return View(view);
        }

        [Authorize(Roles = "Admin")]
        public IActionResult EditState(string username)
        {
            ViewBag.username = username;

            var userState = this.clientRepository.GetClientStateByUserName(username);

            ViewBag.state = userState;

            ViewData["StateId"] = new SelectList(this.aInventoryRepository.GetStates(), "Id", "Name");

            return View();
        }

        [HttpPost]
        public IActionResult EditState(ChangeStateViewModel view)
        {
            this.clientRepository.OnlyUpdateClientState(view);

            if (view.StateId == "4")
            {
                var list = this.viewerRepository.GetThisClientViewersList(view.UserName);

                foreach (var item in list)
                {
                    var getUser = this.userHelper.GetUserByEmail(item.Email);

                    var checkInClients = this.clientRepository.GetClientByUserName(getUser.Email);

                    if (checkInClients == null)
                    {
                        this.clientRepository.OnlyUpdateViewerEmailConfirmedToFalse(getUser);
                    }
                }

                var getUserClient = this.userHelper.GetUserByEmail(view.UserName);

                this.clientRepository.OnlyUpdateViewerEmailConfirmedToFalse(getUserClient);
            }
            else
            {
                var list = this.viewerRepository.GetThisClientViewersList(view.UserName);

                foreach (var item in list)
                {
                    var getUser = this.userHelper.GetUserByEmail(item.Email);

                    var checkInClients = this.clientRepository.GetClientByUserName(getUser.Email);

                    if (checkInClients == null)
                    {
                        this.clientRepository.OnlyUpdateViewerEmailConfirmedToTrue(getUser);
                    }
                }

                var getUserClientTrue = this.userHelper.GetUserByEmail(view.UserName);

                this.clientRepository.OnlyUpdateViewerEmailConfirmedToTrue(getUserClientTrue);
            }

            return RedirectToAction("IndexAdmin", "Clients");
        }

        [NonAction]
        // GET: Clients/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var client = await this.clientRepository.GetByIdAsyncInclude(id.Value);
            if (client == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(client);
        }

        // POST: Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var client = await this.clientRepository.GetByIdAsync(id);
            try
            {
                await this.clientRepository.DeleteAsync(client);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }

            return RedirectToAction(nameof(Index));
        }



        //JSON RESULT
        [HttpPost]
        public async Task<ActionResult> GetData(string Code)
        {

            var codeJoin = Code.Split('-');
            var codeConcat = codeJoin[0] + codeJoin[1];
            var response = await Services.ApiService.GetCities("http://codigospostais.appspot.com", $"/cp7?codigo={codeConcat}");


            var cities = new Codigo();
            cities = (Codigo)response.Result;


            var anotherResponse = await Services.ApiService.GetCitiesGPS("http://codigospostais.appspot.com", $"/cp4?codigo={codeJoin[0]}");

            var codeGps = new CodigoComGps();
            codeGps = (CodigoComGps)anotherResponse.Result;

            var findZipCode = this.zipCodeRepository.GetZipCodeByCode(Code);

            if (findZipCode == null)
            {
                if (cities.arteria != null)
                {
                    var thisZipCode = new ZipCode
                    {
                        City = cities.localidade,
                        Name = cities.arteria,
                        Code = Code,
                        Country = "Portugal",
                        longitude = codeGps.longitude,
                        latitude = codeGps.latitude
                    };
                    await this.zipCodeRepository.CreateAsync(thisZipCode);
                }

            }

            if (cities.arteria == null)
            {
                cities.arteria = "Please insert your street name";
            }

            return Ok(cities.arteria);

        }
    }
}
