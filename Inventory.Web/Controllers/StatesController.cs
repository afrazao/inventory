﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;
using Microsoft.AspNetCore.Authorization;

namespace Inventory.Web.Controllers
{
    [Authorize(Roles ="Admin")]
    public class StatesController : Controller
    {
     
        private readonly IAInventoryRepository aInventoryRepository;

        public StatesController(IAInventoryRepository aInventoryRepository)
        {
            
            this.aInventoryRepository = aInventoryRepository;
        }

        // GET: States
        public IActionResult Index()
        {
            return View(this.aInventoryRepository.GetStates());
        }

        // GET: States/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var state = await this.aInventoryRepository.GetStateById(id.Value);
            if (state == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(state);
        }

        // GET: States/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: States/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] State state)
        {
            if (ModelState.IsValid)
            {
                await this.aInventoryRepository.CreateStateAsync(state);
                return RedirectToAction(nameof(Index));
            }
            return View(state);
        }

        // GET: States/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var state = await this.aInventoryRepository.GetStateById(id.Value);
            if (state == null)
            {
                return RedirectToAction("Error404", "Error");
            }
            return View(state);
        }

        // POST: States/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] State state)
        {
            if (id != state.Id)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await this.aInventoryRepository.UpdateStateAsync(state);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await this.aInventoryRepository.StateExistsAsync(state.Id))
                    {
                        return RedirectToAction("Error404", "Error");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(state);
        }

        // GET: States/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var state = await this.aInventoryRepository.GetStateById(id.Value);
            if (state == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(state);
        }

        // POST: States/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var state = await this.aInventoryRepository.GetStateById(id);
            try
            {
                await this.aInventoryRepository.DeleteStateAsync(state);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }
            return RedirectToAction(nameof(Index));
        }

      
    }
}
