﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Models;
using System.IO;
using Inventory.Web.Data.IRepositories;
using Inventory.Web.Helpers;
using Microsoft.AspNetCore.Authorization;

namespace Inventory.Web.Controllers
{
    public class InventoryArticlesController : Controller
    {
     
        private readonly IAInventoryRepository aInventoryRepository;
        private readonly IArticleRepository articleRepository;
        private readonly IUserHelper userhelper;
        private readonly IClientRepository clientRepository;
        private readonly IViewerRepository viewerRepository;
        public decimal countImobilizado1;
        public decimal countConsumível1;
        public decimal countPerecível1;
        public decimal countPerigoso1;
        public int countCat = 1;
        public int countToId;

        public InventoryArticlesController(IAInventoryRepository aInventoryRepository,IArticleRepository articleRepository, IUserHelper userhelper, IClientRepository clientRepository, IViewerRepository viewerRepository)
        {
            
            this.aInventoryRepository = aInventoryRepository;
            this.articleRepository = articleRepository;
            this.userhelper = userhelper;
            this.clientRepository = clientRepository;
            this.viewerRepository = viewerRepository;
        }

        [Authorize(Roles = "InventoryAdmin, InventoryView, InventoryCRU")]
        // GET: InventoryArticles
        public IActionResult Index(int id)
        {
            var dataContext = this.aInventoryRepository.GetInventoryArticles();
            //var dataContext = _context.InventoryArticles.Include(i => i.Article).Include(i => i.Inventory).ToList();

            var ListArticlesInventory = this.aInventoryRepository.GetInventoryArticles(id);

            List<int> ListArticlesInventoryIds = new List<int>();

            foreach (var item in ListArticlesInventory)
            {
                ListArticlesInventoryIds.Add(item.ArticleId);
            }

            var ListArticles = this.articleRepository.GetArticlesByInventoryArticlesId(ListArticlesInventoryIds);

            ViewBag.dataSource = ListArticlesInventory;

            ViewBag.id = id;

            List<Article> ListArticlesImobilizado = this.articleRepository.GetArticlesByCategory(ListArticles, 1);
            List<Article> ListArticlesConsumível = this.articleRepository.GetArticlesByCategory(ListArticles, 2);
            List<Article> ListArticlesPerecível = this.articleRepository.GetArticlesByCategory(ListArticles, 3);
            List<Article> ListArticlesPerigoso = this.articleRepository.GetArticlesByCategory(ListArticles, 4);

            //var countImobilizado1 = ListArticlesImobilizado.Count();
            //var countConsumível1 = ListArticlesConsumível.Count();
            //var countPerecível1 = ListArticlesPerecível.Count();
            //var countPerigoso1 = ListArticlesPerigoso.Count();

            foreach (var item in ListArticlesImobilizado)
            {
                var getInventoryArticle = this.aInventoryRepository.GetInventoryArticleByArticleId(item.Id);

                countImobilizado1 += getInventoryArticle.Quantity;
            }

            foreach (var item in ListArticlesConsumível)
            {
                var getInventoryArticle = this.aInventoryRepository.GetInventoryArticleByArticleId(item.Id);

                countConsumível1 += getInventoryArticle.Quantity;
            }

            foreach (var item in ListArticlesPerecível)
            {
                var getInventoryArticle = this.aInventoryRepository.GetInventoryArticleByArticleId(item.Id);

                countPerecível1 += getInventoryArticle.Quantity;
            }

            foreach (var item in ListArticlesPerigoso)
            {
                var getInventoryArticle = this.aInventoryRepository.GetInventoryArticleByArticleId(item.Id);

                countPerigoso1 += getInventoryArticle.Quantity;
            }

            double countImobilizado = Convert.ToDouble(countImobilizado1);
            double countConsumível = Convert.ToDouble(countConsumível1);
            double countPerecível = Convert.ToDouble(countPerecível1);
            double countPerigoso = Convert.ToDouble(countPerigoso1);

            var count = countImobilizado + countConsumível + countPerecível + countPerigoso;
            var valueImobilizado = Math.Round((countImobilizado / count) * 100, 0);
            var valueConsumível = Math.Round((countConsumível / count) * 100, 0);
            var valuePerecível = Math.Round((countPerecível / count) * 100, 0);
            var valuePerigoso = Math.Round((countPerigoso / count) * 100, 0);

            List<DoughnutData> chartData = new List<DoughnutData>
            {
                new DoughnutData { xValue = "Immobilized", yValue = valueImobilizado, text = $"{valueImobilizado}%" },
                new DoughnutData { xValue = "Consumable", yValue = valueConsumível, text = $"{valueConsumível}%" },
                new DoughnutData { xValue = "Perishable", yValue = valuePerecível, text = $"{valuePerecível}%" },
                new DoughnutData { xValue = "Dangerous", yValue = valuePerigoso, text = $"{valuePerigoso}%" }
            };

            ViewBag.dataSource = chartData;

            List<object> listdata = new List<object>();

            listdata.Add(new
            {
                id = 1,
                category = "Immobilized",
                name = "",
                barcode = "",
                quantity = "",
                unit = "",
                imageUrl = "",
                child = true,
                expanded = false,
            });

           
            foreach (var item in ListArticlesImobilizado)
            {
                var ia = this.aInventoryRepository.GetInventoryArticleByArticleId(item.Id);

                countCat++;

                if (ia != null)
                {

                    listdata.Add(new
                    {
                        id = countCat,
                        pid = 1,
                        idI = item.Id,
                        name = item.Name,
                        quantity = ia.Quantity,
                        unit = ia.Unit,
                        address = item.Address,
                        imageUrl = item.ImageUrl
                    });
                }
            }

            countToId = countCat + 1;
            countCat++;

            listdata.Add(new
            {
                id = countToId,
                category = "Consumable",
                name = "",
                barcode = "",
                quantity = "",
                unit = "",
                imageUrl = "",
                child = true,
            });

            foreach (var item in ListArticlesConsumível)
            {
                var ia = this.aInventoryRepository.GetInventoryArticleByArticleId(item.Id);

                countCat++;

                if (ia != null)
                {
                    listdata.Add(new
                    {
                        id = countCat,
                        pid = countToId,
                        idI = item.Id,
                        name = item.Name,
                        quantity = ia.Quantity,
                        unit = ia.Unit,
                        address=item.Address,
                        imageUrl = item.ImageUrl,
                    });
                }
            }

            countToId = countCat + 1;
            countCat++;

            listdata.Add(new
            {
                id = countToId,
                category = "Perishable",
                name = "",
                barcode = "",
                quantity = "",
                unit = "",
                imageUrl = "",
                child = true,
            });

            foreach (var item in ListArticlesPerecível)
            {
                var ia = this.aInventoryRepository.GetInventoryArticleByArticleId(item.Id);

                countCat++;

                if (ia != null)
                {
                    listdata.Add(new
                    {
                        id = countCat,
                        pid = countToId,
                        idI = item.Id,
                        name = item.Name,
                        quantity = ia.Quantity,
                        unit = ia.Unit,
                        address=item.Address,
                        imageUrl = item.ImageUrl,
                    });
                }
            }

            countToId = countCat + 1;
            countCat++;

            listdata.Add(new
            {
                id = countToId,
                category = "Dangerous",
                name = "",
                barcode = "",
                quantity = "",
                unit = "",
                imageUrl = "",
                child = true,
            });

            foreach (var item in ListArticlesPerigoso)
            {
                var ia = this.aInventoryRepository.GetInventoryArticleByArticleId(item.Id);

                countCat++;

                if (ia != null)
                {
                    listdata.Add(new
                    {
                        id = countCat,
                        pid = countToId,
                        idI = item.Id,
                        name = item.Name,
                        quantity = ia.Quantity,
                        unit = ia.Unit,
                        address = item.Address,
                        imageUrl = item.ImageUrl,
                    });
                }
            }

            var inventoryName = this.aInventoryRepository.InventoryName(id);
            ViewBag.Name = inventoryName;

            ViewBag.dataSourceData = listdata;

            return View(dataContext);
        }

         
        [NonAction]
        // GET: InventoryArticles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var inventoryArticle = await this.aInventoryRepository.GetInventoryArticleById(id.Value);
            if (inventoryArticle == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(inventoryArticle);
        }

        [NonAction]
        // GET: InventoryArticles/Create
        public IActionResult Create()
        {
            ViewData["ArticleId"] = new SelectList(this.articleRepository.GetAllInclude(), "Id", "Code");
            ViewData["AInventoryId"] = new SelectList(this.aInventoryRepository.GetAll(), "Id", "Name");
            return View();
        }

        // POST: InventoryArticles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,AInventoryId,ArticleId,Unit,Name")] InventoryArticle view)
        {
            if (ModelState.IsValid)
            {


                await this.aInventoryRepository.CreateInventoryArticleAsync(view);
                return RedirectToAction(nameof(Index));

            }
            ViewData["ArticleId"] = new SelectList(this.articleRepository.GetAll(), "Id", "Code", view.ArticleId);
            ViewData["AInventoryId"] = new SelectList(this.aInventoryRepository.GetAll(), "Id", "Name", view.AInventoryId);
            return View(view);
        }


        [NonAction]
        // GET: InventoryArticles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var inventoryArticle = await this.aInventoryRepository.GetInventoryArticleById(id.Value);
            if (inventoryArticle == null)
            {
                return RedirectToAction("Error404", "Error");
            }
           
            ViewData["ArticleId"] = new SelectList(this.articleRepository.GetAll(), "Id", "Code", inventoryArticle.ArticleId);
            ViewData["AInventoryId"] = new SelectList(this.aInventoryRepository.GetAll(), "Id", "Name", inventoryArticle.AInventoryId);
            return View(inventoryArticle);
        }

       

        // POST: InventoryArticles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,AInventoryId,ArticleId,Quantity,Unit,Name")] InventoryArticle view)
        {
            if (id != view.Id)
            {
                return RedirectToAction("Error404", "Error");
            }

            if (ModelState.IsValid)
            {
                try
                {

                  


                    await this.aInventoryRepository.UpdateInventoryArticleAsync(view);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await this.aInventoryRepository.InventoryArticleExistsAsync(view.Id))
                    {
                        return RedirectToAction("Error404", "Error");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ArticleId"] = new SelectList(this.articleRepository.GetAll(), "Id", "Code", view.ArticleId);
            ViewData["AInventoryId"] = new SelectList(this.aInventoryRepository.GetAll(), "Id", "Name", view.AInventoryId);
            return View(view);
        }

        [NonAction]
        // GET: InventoryArticles/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var inventoryArticle = await this.aInventoryRepository.GetInventoryArticleById(id.Value);
            if (inventoryArticle == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(inventoryArticle);
        }

        // POST: InventoryArticles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var inventoryArticle = await this.aInventoryRepository.GetInventoryArticleById(id);
            try
            {
                await this.aInventoryRepository.DeleteInventoryArticleAsync(inventoryArticle);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }
            return RedirectToAction(nameof(Index));
        }

    
    }
}
