﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;

using Inventory.Web.Models;
using Inventory.Web.Helpers;
using System.IO;
using Microsoft.AspNetCore.Authorization;

namespace Inventory.Web.Controllers
{
    public class ArticlesController : Controller
    {
       
        private readonly IArticleRepository articleRepository;
        private readonly ICategoryRepository categoryRepository;
        private readonly IZipCodeRepository zipCodeRepository;
        private readonly IAInventoryRepository aInventoryRepository;

        public class ButtonModel
        {
            public string content { get; set; }
            public string cssClass { get; set; }
        }

        public ArticlesController( IArticleRepository articleRepository,ICategoryRepository categoryRepository, IZipCodeRepository zipCodeRepository,IAInventoryRepository aInventoryRepository)
        {
            
            this.articleRepository = articleRepository;
            this.categoryRepository = categoryRepository;
            this.zipCodeRepository = zipCodeRepository;
            this.aInventoryRepository = aInventoryRepository;
        }

        [NonAction]
        // GET: Articles
        public IActionResult Index()
        {
            var dataContext = this.articleRepository.GetAllInclude();
            var articles = this.articleRepository.GetArticles();

         

            return View(dataContext);
        }




        //JSON RESULT
        [HttpPost]
        public IActionResult GetCode(string Code)
        {

            ViewBag.Code = Code;




            return Ok(Code);

        }


        [Route("generate")]
        public IActionResult Generate(string code)
        {
            ViewBag.Code = code;
            ViewData["CategoryId"] = new SelectList(this.categoryRepository.GetAll(), "Id", "Name");
            ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City");
            return View("Create");
        }

        [Authorize(Roles = "InventoryAdmin, InventoryCRU")]
        // GET: Articles/Details/5
        public async Task<IActionResult> Details(int? id, int idI)
        {

            if(idI!= 0)
            {
                ViewBag.IdI = idI;
            }


            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            ViewBag.id = id;

            var article = await this.articleRepository.GetByIdAsyncInclude(id.Value);
            if (article == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(article);
        }

        [Authorize(Roles = "InventoryAdmin, InventoryCRU")]
        // GET: Articles/Create
        public IActionResult Create(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            ViewBag.id = id;

            ViewData["CategoryId"] = new SelectList(this.categoryRepository.GetAll(), "Id", "Name");
            ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City");
            ViewBag.defaultbutton= new ButtonModel() { cssClass = "e-flat", content = "OK" };
            return View();
        }

        // POST: Articles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CategoryId,Code,Name,ZipCodeId,ImageFile")] ArticlesViewModel view, int id)
        {
           
            string articleQuantity = Request.Form["Quantity"];

            if (articleQuantity == "")
            {
                ModelState.AddModelError(string.Empty, "Insert a Quantity, please.");
                ViewBag.id = id;

                ViewData["CategoryId"] = new SelectList(this.categoryRepository.GetAll(), "Id", "Name");
                ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City");
                ViewBag.defaultbutton = new ButtonModel() { cssClass = "e-flat", content = "OK" };
                return View();
            }

            string addressCheck = Request.Form["Address"];

            if (string.IsNullOrEmpty(addressCheck) || addressCheck == "Please insert your street name")
            {
                ModelState.AddModelError("Address", "Insert a valid address, please.");
                ViewBag.id = id;

                ViewData["CategoryId"] = new SelectList(this.categoryRepository.GetAll(), "Id", "Name");
                ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City");
                ViewBag.defaultbutton = new ButtonModel() { cssClass = "e-flat", content = "OK" };
                return View();
            }





          

       

            string code = Request.Form["ZipCode"];



            if (code == "0000-000" || string.IsNullOrEmpty(code))
            {

                ModelState.AddModelError("ZipCode", "Please insert a valid ZipCode.");
                ViewBag.id = id;

                ViewData["CategoryId"] = new SelectList(this.categoryRepository.GetAll(), "Id", "Name");
                ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City");
                ViewBag.defaultbutton = new ButtonModel() { cssClass = "e-flat", content = "OK" };
                return View();
            }

            string sub = code.Substring(4, 1);

            if (sub != "-")
            {
                ModelState.AddModelError("ZipCode", "Please insert a valid ZipCode.");
                ViewBag.id = id;

                ViewData["CategoryId"] = new SelectList(this.categoryRepository.GetAll(), "Id", "Name");
                ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City");
                ViewBag.defaultbutton = new ButtonModel() { cssClass = "e-flat", content = "OK" };
                return View();
            }

            var getZipCode = this.zipCodeRepository.GetZipCodeByAddress(addressCheck);

            if (getZipCode != null)
            {
                view.ZipCode = getZipCode;
            }

            else
            {
                var zipCode = new ZipCode
                {
                    Code = code,
                    City = "Lisboa",
                    Country = "Portugal",
                    Name = addressCheck,
                    longitude = "0",
                    latitude = "0"
                };

                await this.zipCodeRepository.CreateAsync(zipCode);
                view.ZipCode = zipCode;

            }

            view.Address = addressCheck;


            if (ModelState.IsValid)
            {
               


                var path = string.Empty;
                if (view.ImageFile != null && view.ImageFile.Length > 0)
                {
                    var guid = Guid.NewGuid().ToString();
                    var file = $"{guid}.jpg";
                    path = Path.Combine(
                        Directory.GetCurrentDirectory(),
                        "wwwroot\\images\\Articles",
                        file);


                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await view.ImageFile.CopyToAsync(stream);
                    }


                    path = $"~/images/Articles/{file}";

                }

               

                var article = this.ToArticle(view, path);

                
                var confirmCodeExists = this.articleRepository.FindArticleByCode(Request.Form["Code"]);
                if(confirmCodeExists== null)
                {
                    var qrCode = GenerateQrCode.GetQRCode(Request.Form["Code"]);
                    var image = qrCode.GetGraphic(5);
                    var convert = ImageConverter.imageToByteArray(image);
                    article.BarCode = convert;




                    await this.articleRepository.CreateAsync(article);
                    var Iarticle = new InventoryArticle
                    {
                        
                        
                        AInventoryId = id,
                        ArticleId = this.articleRepository.GetArticleId(article.Code),
                             
                        Quantity = decimal.Parse(articleQuantity),
                        Unit = "1",
                        Name = article.Name,
                    };

                    await this.aInventoryRepository.CreateInventoryArticleAsync(Iarticle);
                    return RedirectToAction("Index", "InventoryArticles", new { id = id });

                }
                else
                {
                    ModelState.AddModelError("Code", "This Code already exists");
                    ViewData["CategoryId"] = new SelectList(this.categoryRepository.GetAll(), "Id", "Name", view.CategoryId);
                    ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City", view.ZipCodeId);
                    return View(view);
                }
             
            
            }
            ViewData["CategoryId"] = new SelectList(this.categoryRepository.GetAll(), "Id", "Name", view.CategoryId);
            ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City", view.ZipCodeId);
            return View(view);
        }

        private Article ToArticle(ArticlesViewModel view, string path)
        {
            return new Article
            {
                CategoryId = view.CategoryId,
                Category = view.Category,
                Code = view.Code,
                BarCode = view.BarCode,
                ZipCode = view.ZipCode,
                ZipCodeId = view.ZipCodeId,
                Name = view.Name,
                Address=view.Address,
                ImageUrl = path               
            };
        }

        [Authorize(Roles = "InventoryAdmin, InventoryCRU")]
        // GET: Articles/Edit/5
        public async Task<IActionResult> Edit(int? id, int idI)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var article = await this.articleRepository.GetByIdAsync(id.Value);
            if (article == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            var view = this.ToArticlesViewModel(article);

            ViewBag.idI = idI;

            ViewBag.id = id;

            var inventoryArticle = this.aInventoryRepository.GetInventoryArticleByArticleId(article.Id);

            var quantity = inventoryArticle.Quantity;

            ViewBag.Quant = quantity;

            ViewData["CategoryId"] = new SelectList(this.categoryRepository.GetAll(), "Id", "Name", article.CategoryId);
            ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "Name", article.ZipCodeId);
            return View(view);
        }

        private ArticlesViewModel ToArticlesViewModel(Article article)
        {
            return new ArticlesViewModel
            {
                Id = article.Id,
                CategoryId = article.CategoryId,
                Category = article.Category,
                Code = article.Code,
                BarCode = article.BarCode,
                ZipCode = article.ZipCode,
                ZipCodeId = article.ZipCodeId,
                Name = article.Name,
                Address=article.Address,
                ImageUrl = article.ImageUrl
            };
        }

        // POST: Articles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,CategoryId,Code,Name,ZipCodeId,ImageFile")] ArticlesViewModel view, int idI)
        {
            if (id != view.Id)
            {
                return RedirectToAction("Error404", "Error");
            }

            string articleQuantity = Request.Form["Quantity"];

            if (articleQuantity == "")
            {
                ModelState.AddModelError(string.Empty, "Insert a Quantity, please.");
                var article = await this.articleRepository.GetByIdAsync(id);
                if (article == null)
                {
                    return RedirectToAction("Error404", "Error");
                }

                var viewCheck = this.ToArticlesViewModel(article);

                ViewBag.idI = idI;

                ViewBag.id = id;

                var inventoryArticle = this.aInventoryRepository.GetInventoryArticleByArticleId(article.Id);

                var quantity = inventoryArticle.Quantity;

                ViewBag.Quant = quantity;

                ViewData["CategoryId"] = new SelectList(this.categoryRepository.GetAll(), "Id", "Name", article.CategoryId);
                ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "Name", article.ZipCodeId);
                return View(viewCheck);
            }

            if (ModelState.IsValid)
            {
                var address = this.articleRepository.GetArticleAddressById(id);
                view.Address = address;
                try
                {
                    var path = view.ImageUrl;

                    if (view.ImageFile != null && view.ImageFile.Length > 0)
                    {
                        var guid = Guid.NewGuid().ToString();
                        var file = $"{guid}.jpg";
                        path = Path.Combine(
                            Directory.GetCurrentDirectory(),
                             "wwwroot\\images\\Articles",
                            file);


                        using (var stream = new FileStream(path, FileMode.Create))
                        {
                            await view.ImageFile.CopyToAsync(stream);
                        }

                        //TODO:falta fazer o delete das imagens quando faz delete

                        path = $"~/images/Articles/{file}";

                    }


                    var article = this.ToArticle(view, path);
                    article.Id = view.Id;
                    var art = await this.articleRepository.GetByIdAsync(id);
                    article.ZipCodeId = art.ZipCodeId;
                    article.CategoryId = art.CategoryId;
                    var qrCode = GenerateQrCode.GetQRCode(Request.Form["Code"]);
                    var image = qrCode.GetGraphic(5);
                    var convert = ImageConverter.imageToByteArray(image);
                    article.BarCode = convert;
                    await this.articleRepository.UpdateAsync(article);

                    
                   
                    var inventoryArticle = this.aInventoryRepository.GetInventoryArticleByArticleId(article.Id);
                    inventoryArticle.Quantity = decimal.Parse(articleQuantity);
                    await this.aInventoryRepository.UpdateInventoryArticleAsync(inventoryArticle);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await this.articleRepository.ExistsAsync(view.Id))
                    {
                        return RedirectToAction("Error404", "Error");
                    }
                    else
                    {
                        throw;
                    }
                }
                //TODO:perceber porque é que o id não passa
                return RedirectToAction("Index", "InventoryArticles", new { id = idI });
            }
            ViewData["CategoryId"] = new SelectList(this.categoryRepository.GetAll(), "Id", "Name", view.CategoryId);
            ViewData["ZipCodeId"] = new SelectList(this.zipCodeRepository.GetAll(), "Id", "City", view.ZipCodeId);
            return View(view);
        }

        [Authorize(Roles = "InventoryAdmin")]
        // GET: Articles/Delete/5
        public async Task<IActionResult> Delete(int? id, int idI)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }


            var article = await this.articleRepository.GetByIdAsyncInclude(id.Value);
            if (article == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            ViewBag.idI = idI;

            ViewBag.id = id;

            return View(article);
        }

        // POST: Articles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var inventoryArticle = this.aInventoryRepository.GetInventoryArticleByArticleId(id);
            try
            {
                await this.aInventoryRepository.DeleteInventoryArticleAsync(inventoryArticle);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }
            var idI = inventoryArticle.AInventoryId;
            var article = await this.articleRepository.GetByIdAsync(id);
            try
            {
                await this.articleRepository.DeleteAsync(article);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }
            return RedirectToAction("Index", "InventoryArticles", new { id = idI });
        }
    }
}
