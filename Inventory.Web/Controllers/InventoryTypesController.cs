﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;
using Microsoft.AspNetCore.Authorization;

namespace Inventory.Web.Controllers
{
    [Authorize(Roles ="Admin")]
    public class InventoryTypesController : Controller
    {
        
        private readonly IAInventoryRepository aInventoryRepository;

        public InventoryTypesController(IAInventoryRepository aInventoryRepository)
        {
          
            this.aInventoryRepository = aInventoryRepository;
        }

        // GET: InventoryTypes
        public IActionResult Index()
        {
            return View(this.aInventoryRepository.GetInventoryTypes());
        }

        // GET: InventoryTypes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var inventoryType = await this.aInventoryRepository.GetInventoryTypeById(id.Value);
            if (inventoryType == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(inventoryType);
        }

        // GET: InventoryTypes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: InventoryTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name")] InventoryType inventoryType)
        {
            if (ModelState.IsValid)
            {
                await this.aInventoryRepository.CreateInventoryTypeAsync(inventoryType);
                return RedirectToAction(nameof(Index));
            }
            return View(inventoryType);
        }

        // GET: InventoryTypes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var inventoryType = await this.aInventoryRepository.GetInventoryTypeById(id.Value);
            if (inventoryType == null)
            {
                return RedirectToAction("Error404", "Error");
            }
            return View(inventoryType);
        }

        // POST: InventoryTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name")] InventoryType inventoryType)
        {
            if (id != inventoryType.Id)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            if (ModelState.IsValid)
            {
                try
                {
                  await this.aInventoryRepository.UpdateInventoryTypeAsync(inventoryType);


                    //TODO:acabar este
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await this.aInventoryRepository.InventoryTypeExistsAsync(inventoryType.Id))
                    {
                        return RedirectToAction("Error404", "Error");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(inventoryType);
        }

        // GET: InventoryTypes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var inventoryType = await this.aInventoryRepository.GetInventoryTypeById(id.Value);
            if (inventoryType == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(inventoryType);
        }

        // POST: InventoryTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var inventoryType = await this.aInventoryRepository.GetInventoryTypeById(id);
            try
            {
                await this.aInventoryRepository.DeleteInventoryTypeAsync(inventoryType);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }
            return RedirectToAction(nameof(Index));
        }

      
    }
}
