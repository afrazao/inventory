﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Inventory.Web.Data;
using Inventory.Web.Data.Entities;
using Inventory.Web.Data.IRepositories;
using Microsoft.AspNetCore.Authorization;

namespace Inventory.Web.Controllers
{
    public class ViewerInventoriesController : Controller
    {
       
        private readonly IViewerRepository viewerRepository;
        private readonly IAInventoryRepository aInventoryRepository;
        private readonly IClientRepository clientRepository;

        public ViewerInventoriesController(IViewerRepository viewerRepository, IAInventoryRepository aInventoryRepository, IClientRepository clientRepository)
        {
          
            this.viewerRepository = viewerRepository;
            this.aInventoryRepository = aInventoryRepository;
            this.clientRepository = clientRepository;
        }

        [Authorize(Roles = "InventoryAdmin")]
        // GET: ViewerInventories
        public IActionResult Index(int id)
        {
            var ViewersByInventoryList = this.viewerRepository.GetIdViewersList(id);

            var ViewersToList = this.viewerRepository.GetViewersList(ViewersByInventoryList);

            List<object> listdata = new List<object>();

            foreach (var item in ViewersToList)
            {
                listdata.Add(new
                {
                    text = item.Email,
                    id = item.Id,
                    imgUrl = "",
                    icon = this.clientRepository.GetClientFirstLetter(item.Email),
                    //contact = item.Contact,
                    //category = "Information",
                    child = new List<object>() { new { text = "Remove", id = item.Id, imgUrl = "", icon = this.clientRepository.GetClientFirstLetter(item.Email), category = "Information", child = "" },
                    //new { text = item.StateId, id = item.Id, imgUrl = "", icon = "N", category = "Information", child = "" },
                    //new { text = "Delete", id = item.Id, imgUrl = "", icon = "N", category = "Information", child = "" }
                    }
                });
            }

            ViewBag.listData = listdata;

            ViewBag.IdInventory = id;

            return View();

        }

        [Authorize(Roles = "InventoryAdmin, Admin")]
        // GET: Viewers
        public IActionResult IndexAdmin(int id, string username)
        {
            ViewBag.username = username;

            var ViewersByInventoryList = this.viewerRepository.GetIdViewersList(id);

            var ViewersToList = this.viewerRepository.GetViewersList(ViewersByInventoryList);

            List<object> listdata = new List<object>();

            foreach (var item in ViewersToList)
            {
                listdata.Add(new
                {
                    text = item.Email,
                    id = item.Id,
                    imgUrl = "",
                    icon = this.clientRepository.GetClientFirstLetter(item.Email),
                    //contact = item.Contact,
                    //category = "Information",
                    child = new List<object>() { new { text = item.Name + ' ' + item.LastName , id = item.Id, imgUrl = "", icon = this.clientRepository.GetClientFirstLetter(item.Email), category = "Information", child = "" },
                    //new { text = item.StateId, id = item.Id, imgUrl = "", icon = "N", category = "Information", child = "" },
                    //new { text = "Inventories", id = item.Id, imgUrl = "", icon = "I", category = "Information", child = "" }
                    }
                });
            }

            ViewBag.listData = listdata;
            return View();
        }

        [NonAction]
        // GET: ViewerInventories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var viewerInventories = await this.viewerRepository.GetViewerInventoriesById(id.Value);
            if (viewerInventories == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(viewerInventories);
        }

        [NonAction]
        // GET: ViewerInventories/Create
        public IActionResult Create()
        {
            ViewData["AInventoryId"] = new SelectList(this.aInventoryRepository.GetAll(), "Id", "Name");
            ViewData["ViewerId"] = new SelectList(this.viewerRepository.GetAll(), "Id", "Id");
            return View();
        }

        // POST: ViewerInventories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,AInventoryId,ViewerId,Name")] ViewerInventories viewerInventories)
        {
            if (ModelState.IsValid)
            {
                await this.viewerRepository.CreateViewerInventoryAsync(viewerInventories);
                return RedirectToAction(nameof(Index));
            }
            ViewData["AInventoryId"] = new SelectList(this.aInventoryRepository.GetAll(), "Id", "Name", viewerInventories.AInventoryId);
            ViewData["ViewerId"] = new SelectList(this.viewerRepository.GetAll(), "Id", "Name", viewerInventories.ViewerId);
            return View(viewerInventories);
        }

        [NonAction]
        // GET: ViewerInventories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var viewerInventories = await this.viewerRepository.GetViewerInventoriesById(id.Value);
            if (viewerInventories == null)
            {
                return RedirectToAction("Error404", "Error");
            }
            ViewData["AInventoryId"] = new SelectList(this.aInventoryRepository.GetAll(), "Id", "Name", viewerInventories.AInventoryId);
            ViewData["ViewerId"] = new SelectList(this.viewerRepository.GetAll(), "Id", "Name", viewerInventories.ViewerId);
            return View(viewerInventories);
        }

        // POST: ViewerInventories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,AInventoryId,ViewerId,Name")] ViewerInventories viewerInventories)
        {
            if (id != viewerInventories.Id)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await this.viewerRepository.UpdateViewerInventoriesAsync(viewerInventories);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!await this.viewerRepository.ViewerInventoriesExistsAsync(viewerInventories.Id))
                    {
                        return RedirectToAction("Error404", "Error");
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AInventoryId"] = new SelectList(this.aInventoryRepository.GetAll(), "Id", "Name", viewerInventories.AInventoryId);
            ViewData["ViewerId"] = new SelectList(this.viewerRepository.GetAll(), "Id", "Name", viewerInventories.ViewerId);
            return View(viewerInventories);
        }

        [NonAction]
        // GET: ViewerInventories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("MyBadRequest", "Error");
            }

            var viewerInventories = await this.viewerRepository.GetViewerInventoriesById(id.Value);
            if (viewerInventories == null)
            {
                return RedirectToAction("Error404", "Error");
            }

            return View(viewerInventories);
        }

        // POST: ViewerInventories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var viewerInventories = await this.viewerRepository.GetViewerInventoriesById(id);
            try
            {
                await this.viewerRepository.DeleteViewerInventoriesAsync(viewerInventories);
            }
            catch (Exception)
            {

                return RedirectToAction("CannotDelete", "Error");
            }
            return RedirectToAction(nameof(Index));
        }

       
    }
}
