﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Common.Models
{
    public class Invent
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("state")]
        public State State { get; set; }

     

        [JsonProperty("inventoryType")]
        public InventoryType InventoryType { get; set; }

      

        [JsonProperty("name")]
        public string Name { get; set; }



    

        [JsonProperty("imageFullPath")]
        public string ImageFullPath { get; set; }


        [JsonProperty("client")]
        public Client Client { get; set; }


        [JsonProperty("imageUrl")]
        public string ImageUrl { get; set; }

     

    }
}
