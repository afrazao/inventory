﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Common.Models
{
    public class TokenRequest
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
