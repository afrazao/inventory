﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.Common.Models
{
    public class TokenResponse
    {
        [JsonProperty("token")] //Isto serve porque o pedido é feito em JS por isso "token"
        public string Token { get; set; }

        [JsonProperty("expiration")]
        public string Expiration { get; set; }
    }
}
