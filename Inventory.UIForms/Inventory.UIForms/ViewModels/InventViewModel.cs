﻿using GalaSoft.MvvmLight.Command;
using Inventory.Common.Models;
using Inventory.Common.Services;
using Inventory.UIForms.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;


namespace Inventory.UIForms.ViewModels
{
    public class InventViewModel: INotifyPropertyChanged
    {

        private ApiService apiService;

        private ObservableCollection<Invent> _invents;

        public event PropertyChangedEventHandler PropertyChanged;

       

        public ObservableCollection<Invent> Invents
        {
            get { return this._invents; }

            set
            {
                if(this._invents != value)
                {
                    this._invents = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Invents"));
                }
            }
        }



        public InventViewModel()
        {
            this.apiService = new ApiService();
            this.LoadInvent();
        }

      
        public ICommand OpenInventoryCommand
        {
            get
            {

                return new RelayCommand(Items);



            }
        }




        private async void Items()
        {
            MainViewModel.GetInstance().Inventario = new InventarioViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new InventarioPage());
        }





        private async void LoadInvent()
        {
            //var response = await this.apiService.GetListAsync<Invent>(
            //    "https://inventoryweb20190513042802.azurewebsites.net/",
            //    "/api",
            //    "/AInventories");

            var url = Application.Current.Resources["UrlAPI"].ToString();
            var response = await this.apiService.GetListAsync<Invent>(
                url,
                "/api",
                "/AInventories",
                "bearer",
                MainViewModel.GetInstance().Token.Token);

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                return;

            }

            var myInvents = (List<Invent>)response.Result;
            this.Invents = new ObservableCollection<Invent>(myInvents);










        }








    }
}
