﻿using GalaSoft.MvvmLight.Command;
using Inventory.Common.Models;
using Inventory.Common.Services;
using Inventory.UIForms.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Inventory.UIForms.ViewModels
{
    public class LoginViewModel:INotifyPropertyChanged
    {
        private bool isRunning;
        private bool isEnabled;

        private ApiService apiService;

        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsRunning
        {
            get { return this.isRunning; }

            set
            {
                if (this.isRunning != value)
                {
                    this.isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
        }

        public bool IsEnabled
        {
            get { return this.isEnabled; }

            set
            {
                if (this.isEnabled != value)
                {
                    this.isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEnabled"));
                }
            }
        }

        public string Email { get; set; }


        public string Password { get; set; }


        public ICommand LoginCommand
        {
            get
            {
                return new RelayCommand(Login);
            }

        }


        public ICommand AboutCommand
        {


            get
            {
                return new RelayCommand(About);
            }



        }







        public LoginViewModel()
        {

            this.apiService = new ApiService();
            //this.netService = new NetService();

            //this.Email = "jose.britoesc@gmail.com";
            //this.Password = "123456";
            this.isEnabled = true;
        }



        private async void About()
        {
            MainViewModel.GetInstance().About = new AboutViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new AboutPage());
        }









        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter an Email",
                    "Accept"
                    );
                return;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    "You must enter a Password",
                    "Accept"
                    );
                return;
            }


            //if (!this.Email.Equals("jose.britoesc@gmail.com") || !this.Password.Equals("123456"))
            //{
            //    await Application.Current.MainPage.DisplayAlert(
            //       "Error",
            //       "User or password wrong",
            //       "Accept"
            //       );
            //    return;
            //}

            //await Application.Current.MainPage.DisplayAlert(

            //    "Welcome",
            //        "InventYOURy, ltd",
            //        "Accept"


            //    );

            this.isRunning = true;
            this.isEnabled = false;

            var request = new TokenRequest //Aqui é o pedido que escrevemos no Postman (Post, Body (Raw, application/json)
            {
                Password = this.Password,
                Username = this.Email
            };

            var url = Application.Current.Resources["UrlAPI"].ToString();
            var response = await this.apiService.GetTokenAsync(
                url,
                "/Account",
                "/CreateToken",
                request);

            this.isRunning = false;
            this.isEnabled = true;

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Email or Password Incorrect", "Accept");

                return;
            }

            var token = (TokenResponse)response.Result;
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Token = token;             
            MainViewModel.GetInstance().Invents = new InventViewModel();
            await Application.Current.MainPage.Navigation.PushAsync(new InventPage());


            //MainViewModel.GetInstance().Firs = new FirsViewModel();
            //await Application.Current.MainPage.Navigation.PushAsync(new FirsPage());




        }
    }
}
