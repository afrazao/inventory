﻿using Inventory.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Inventory.UIForms.ViewModels
{
    public class MainViewModel
    {
        private static MainViewModel instance;

        public TokenResponse Token { get; set; }

        public LoginViewModel Login { get; set; }

        public InventViewModel Invents { get; set; }

        public AboutViewModel About { get; set; }
        
        public FirsViewModel Firs { get; set; }

        public InventarioViewModel Inventario { get; set; }

        public MainViewModel()
        {
            //this.Login = new LoginViewModel();



            instance = this;

        }

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }
            return instance;
        }








    }
}
