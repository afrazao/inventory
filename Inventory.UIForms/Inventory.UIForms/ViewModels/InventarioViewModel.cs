﻿using Inventory.Common.Models;
using Inventory.Common.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace Inventory.UIForms.ViewModels
{
    public class InventarioViewModel : INotifyPropertyChanged
    {
        private ApiService apiService;

        private ObservableCollection<Invent> _invents;

        public event PropertyChangedEventHandler PropertyChanged;


        public ObservableCollection<Invent> Invents
        {
            get { return this._invents; }

            set
            {
                if (this._invents != value)
                {
                    this._invents = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Invents"));
                }
            }
        }



        public InventarioViewModel()
        {
            this.apiService = new ApiService();
            this.LoadThings();
        }


        private async void LoadThings()
        {
            var response = await this.apiService.GetListAsync<Invent>(
                "https://inventoryweb20190513042802.azurewebsites.net/",
                "/api",
                "/AInventories"

                );

            if (!response.IsSuccess)
            {
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "Accept");
                return;

            }

            var myInvents = (List<Invent>)response.Result;
            this.Invents = new ObservableCollection<Invent>(myInvents);









        }
    }
}
